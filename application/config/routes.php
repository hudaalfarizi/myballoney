<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'User/Dashboard/dashboard';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
** path ---- > folder / controller / method
*/

// login Before Dashboard Admin
$route['auth/login'] = 'Auth/Login/index';
$route['auth/Logout'] = 'Auth/Logout/do_logout';

// Admin Pages Dashboard
$route['admin'] = 'Admin/Dashboard/dashboard';

// Admin Pages Home
$route['admin/home'] = 'Admin/Dashboard/home';

// Admin Pages -> Transaksi Menu
$route['admin/transaksi'] = 'Admin/Transaksi/index';
$route['admin/transaksiMenunggu'] = 'Admin/Transaksi/transaksiMenunggu';
$route['admin/transaksiPembayaran'] = 'Admin/Transaksi/transaksiPembayaran';
$route['admin/transaksiProses'] = 'Admin/Transaksi/transaksiProses';
$route['admin/transaksiSelesai'] = 'Admin/Transaksi/transaksiSelesai';

$route['admin/transaksiDetail'] = 'Admin/Transaksi/transaksiDetail';
$route['admin/transaksiEdit'] = 'Admin/Transaksi/transaksiEdit';

// Admin Pages -> Produk Menu
$route['admin/produk'] = 'Admin/Produk/index';
// Admin Pages Produk Menu -> Master Menu
$route['admin/master'] = 'Admin/Produk/master';
$route['admin/detailMaster/(:any)'] = 'Admin/Produk/detailMaster/$1';
$route['admin/editMaster/(:any)'] = 'Admin/Produk/editMaster/$1';

$route['admin/additionalOptions'] = 'Admin/Produk/additionalOptions';
$route['admin/detailAdditional/(:any)'] = 'Admin/Produk/detailAdditional/$1';
$route['admin/editAdditional/(:any)'] = 'Admin/Produk/editAdditional/$1';

$route['admin/importantNote'] = 'Admin/Produk/importantNote';
$route['admin/detailImportant/(:any)'] = 'Admin/Produk/detailImportant/$1';
$route['admin/editImportant/(:any)'] = 'Admin/Produk/editImportant/$1';

// Admin Pages Produk Menu -> Komponen Menu
$route['admin/font'] = 'Admin/Produk/font';
$route['admin/detailFont/(:any)'] = 'Admin/Produk/detailFont/$1';
$route['admin/editFont/(:any)'] = 'Admin/Produk/editFont/$1';

$route['admin/kategori'] = 'Admin/Produk/kategori';
$route['admin/detailKategori/(:any)'] = 'Admin/Produk/detailKategori/$1';
$route['admin/editKategori/(:any)'] = 'Admin/Produk/editKategori/$1';

$route['admin/komponen'] = 'Admin/Produk/komponen';
$route['admin/detailKomponen/(:any)'] = 'Admin/Produk/detailKomponen/$1';
$route['admin/editKomponen/(:any)'] = 'Admin/Produk/editKomponen/$1';

$route['admin/produkMenuAdditionalDetail/(:any)'] = 'Admin/Produk/produkMenuAdditionalDetail/$1';
// $route['admin/produkMenuAdditionalEdit'] = 'Admin/Produk/produkMenuAdditionalEdit';
$route['admin/produkMenuImportantDetail/(:any)'] = 'Admin/Produk/produkMenuImportantDetail/$1';
// $route['admin/produkMenuImportantEdit'] = 'Admin/Produk/produkMenuImportantEdit';
$route['admin/produkMenuKomponenDetail/(:any)'] = 'Admin/Produk/produkMenuKomponenDetail/$1';
$route['admin/produkMenuKomponenEdit/(:any)'] = 'Admin/Produk/produkMenuKomponenEdit/$1';

// Admin Pages Produk Menu -> Produk Menu
$route['admin/produkMenu'] = 'Admin/Produk/produkMenu';
$route['admin/dataProdukMenu'] = 'Admin/Produk/dataProdukMenu';
$route['admin/produkMenuDetail/(:any)'] = 'Admin/Produk/produkMenuDetail/$1';
$route['admin/produkMenuEdit/(:any)'] = 'Admin/Produk/produkMenuEdit/$1';

$route['admin/produkMenuAdditionalDetail_menuproduk/(:any)'] = 'Admin/Produk/produkMenuAdditionalDetail_menuproduk/$1';
$route['admin/produkMenuImportantDetail_menuproduk/(:any)'] = 'Admin/Produk/produkMenuImportantDetail_menuproduk/$1';
$route['admin/produkMenuKomponenDetail_menuproduk/(:any)'] = 'Admin/Produk/produkMenuKomponenDetail_menuproduk/$1';
$route['admin/produkMenuKomponenEdit_menuproduk/(:any)'] = 'Admin/Produk/produkMenuKomponenEdit_menuproduk/$1';

// Admin Pages -> Member Menu
$route['admin/member'] = 'Admin/Member/index';
$route['admin/detailMember/(:any)'] = 'Admin/Member/detailMember/$1';
$route['admin/riwayatTransaksi'] = 'Admin/Member/riwayatTransaksi';
$route['admin/riwayatDetail'] = 'Admin/Member/riwayatDetail';

// Admin Pages -> Laporan Menu
$route['admin/laporan'] = 'Admin/Laporan/index';
$route['admin/lapTransaksiTabel'] = 'Admin/Laporan/lapTransaksiTabel';
$route['admin/lapTransaksiGrafik'] = 'Admin/Laporan/lapTransaksiGrafik';

$route['admin/lapKeuanganTabel'] = 'Admin/Laporan/lapKeuanganTabel';
$route['admin/lapKeuanganGrafik'] = 'Admin/Laporan/lapKeuanganGrafik';

$route['admin/lapProdukTabel'] = 'Admin/Laporan/lapProdukTabel';
$route['admin/lapProdukGrafik'] = 'Admin/Laporan/lapProdukGrafik';

// Admin My Account Menu
$route['admin/profile'] = 'Admin/Myaccount/profile';
$route['admin/editProfile'] = 'Admin/Myaccount/editProfile';
// Admin My Account -> Pengguna Menu
$route['admin/dataPengguna'] = 'Admin/Myaccount/dataPengguna';
$route['admin/tambahPengguna'] = 'Admin/Myaccount/tambahPengguna';
$route['admin/detailPengguna/(:any)'] = 'Admin/Myaccount/detailPengguna/$1';
$route['admin/editPengguna/(:any)'] = 'Admin/Myaccount/editPengguna/$1';

// Admin Web Profile Menu
$route['admin/webProfile'] = 'Admin/Webprofile/aboutUs';
$route['admin/howToOrder'] = 'Admin/Webprofile/howToOrder';
$route['admin/contactUs'] = 'Admin/Webprofile/contactUs';
$route['admin/galeri'] = 'Admin/Webprofile/galeri';

/*
** path ---- > folder / controller / method
*/

############## USER ##############

// // AUTH USER
// $route['auth/login'] = 'Auth/Authpengguna/index';
// $route['authpengguna/Logout'] = 'Auth/Authpengguna/do_logout';

// User Dashboard Pages 
$route['user'] = 'User/Dashboard/dashboard';
$route['user/aboutUs'] = 'User/Dashboard/aboutUs';
$route['user/howToOrder'] = 'User/Dashboard/howToOrder';
$route['user/gallery'] = 'User/Dashboard/gallery';
$route['user/contactUs'] = 'User/Dashboard/contactUs';

// User Produk Pages
$route['produk'] = 'User/Produk/dashboard';
$route['produk/detailProduk/(:any)'] = 'User/Produk/detailProduk/$1';

// User Shopping Chart
$route['shopcart'] = 'User/Shopcart/dashboard';
$route['shopcart/detail'] = 'User/Shopcart/detailShopCart';
$route['shopcart/edit'] = 'User/Shopcart/editShopCart';

// User Transaction Menu
$route['transaction'] = 'User/Transaction/dashboard';
$route['transaction/transactionMenunggu'] = 'User/Transaction/transactionMenunggu';
$route['transaction/transactionPembayaran'] = 'User/Transaction/transactionPembayaran';
$route['transaction/transactionProses'] = 'User/Transaction/transactionProses';
$route['transaction/transactionDetail'] = 'User/Transaction/transactionDetail';
$route['transaction/transactionEdit'] = 'User/Transaction/transactionEdit';

// User History Menu
$route['history'] = 'User/History/dashboard';
$route['history/historySelesai'] = 'User/History/historySelesai';
$route['history/historySelesaiDetail'] = 'User/History/historySelesaiDetail';

// User Myaccount Menu
$route['myAccount/(:any)'] = 'User/Myaccount/dashboard/$1';

//instagram
$route['instagram'] = 'User/Dashboard/instagram';

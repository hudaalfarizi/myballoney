<!DOCTYPE html>
<html>

<head>
	<title>Apps - Dashboard</title>
	<!-- CSS External -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/external/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/external/fontawesome-free-5.13.0-web/css/all.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/external/summernote/summernote-lite.min.css">
	<!-- CSS Internal -->
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/internal/css/form.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/internal/css/content.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/internal/css/button_action.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/internal/css/modal.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/internal/css/loading.css">
	<!-- Javascript External -->
	<script src="<?= base_url() ?>assets/external/js/jquery.js"></script>
	<script src="<?= base_url() ?>assets/external/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/external/js/Chart.js"></script>
	<script src="<?= base_url() ?>assets/external/js/highcharts.js"></script>
	<script src="<?= base_url() ?>assets/external/js/highcharts-exporting.js"></script>
	<script src="<?= base_url() ?>assets/external/js/highcharts-export-data.js"></script>
	<script src="<?= base_url() ?>assets/external/js/highcharts-accessibility.js"></script>
	<script src="<?= base_url() ?>assets/external/summernote/summernote-lite.min.js"></script>
	<script type="text/javascript">
		function hapus() {
			if (!confirm('Delete?')) {
				event.preventDefault();
			}
		}
	</script>
</head>

<div class="footer-container">
	<label>Nine Cloud 2019</label>
</div>
</body>
<!-- Javascript Internal -->
<script src="<?= base_url() ?>assets/internal/js/button_action.js"></script>
<script src="<?= base_url() ?>assets/internal/js/modal.js"></script>
<script src="<?= base_url() ?>assets/internal/js/balloney.js"></script>
<script src="<?= base_url(); ?>assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url() ?>assets/internal/js/general.js"></script>
<script src="<?= base_url() ?>assets/internal/js/line-chart.js"></script>
<script src="<?= base_url() ?>assets/internal/js/chart-bar.js"></script>
<script>
	$('#summernote').summernote({
		tabsize: 2,
		height: 200,
		toolbar: [
			['style', ['style']],
			['font', ['bold', 'underline', 'clear']],
			['color', ['color']],
			['para', ['ul', 'ol', 'paragraph']],
			['table', ['table']],
			['insert', ['link', 'picture', 'video']],
			['view', ['fullscreen', 'codeview', 'help']]
		]
	});
</script>


</html>

	<!-- footer Menu -->
	<div class="fixed-bottom">
		<button id="toggle" type="button" class="btn btn-outline-danger ">
			<i class="fas fa-bars"></i>
		</button>
		<button id="topBtn" class="btn btn-outline-danger">
			<i class="fas fa-sort-up"></i>
		</button>
	</div>

	<!-- footer -->
	<div class="footer">
		<div class="content-footer">
			<label>ballooney &#169; 2020</label>
		</div>
	</div>
	</body>

	<!-- Javascript External -->
	<script src="<?= base_url() ?>assets/external/js/jquery.js"></script>
	<script src="<?= base_url() ?>assets/external/js/bootstrap.min.js"></script>
	<script src="<?= base_url() ?>assets/internal/js/button_action.js"></script>
	<script src="<?= base_url() ?>assets/internal/js/modal.js"></script>
	<script src="<?= base_url() ?>assets/internal/js/balloney.js"></script>
	<script src="<?= base_url(); ?>assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
	<script src="<?= base_url() ?>assets/internal/js/general.js"></script>
	<script src="<?= base_url() ?>assets/internal/js/line-chart.js"></script>
	<script src="<?= base_url() ?>assets/internal/js/content_frontend.js"></script>
	<!-- external js -->
	<script src="<?= base_url() ?>assets/external/js/jquery.elevateZoom-3.0.8.min.js"></script>
	<script>
		$("#zoom").elevateZoom();
	</script>

	</html>

<div class="footer-container">
	<label>Nine Cloud 2019</label>
</div>
</body>
<!-- Javascript External -->
<script src="<?= base_url() ?>assets/external/js/jquery.js"></script>
<script src="<?= base_url() ?>assets/external/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>assets/external/js/Chart.js"></script>
<!-- Javascript Internal -->
<script src="<?= base_url() ?>assets/internal/js/button_action.js"></script>
<script src="<?= base_url() ?>assets/internal/js/modal.js"></script>
<script src="<?= base_url() ?>assets/internal/js/balloney.js"></script>
<script src="<?= base_url() ?>assets/internal/js/general.js"></script>
<script src="<?= base_url(); ?>assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>
<script src="<?= base_url() ?>assets/internal/js/chart-bar.js"></script>
<script src="<?= base_url() ?>assets/external/js/jquery.elevateZoom-3.0.8.min.js"></script>
<script>
	$("#zoom").elevateZoom();
</script>

</html>

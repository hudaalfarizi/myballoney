<?php

function upload_photo_banner($files)
{
	$ci = &get_instance();

	$name = time() . $files['pic']['name'];
	$nama = $name;
	$config['upload_path'] = FCPATH . 'assets/image/uploads_photo/banner/';
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	// $config['max_size'] = 2000;
	// $config['max_width'] = 1024;
	// $config['max_height'] = 768;
	$config['file_name'] = $nama;
	$ci->upload->initialize($config);
	if (!$ci->upload->do_upload('pic')) {
		return false;
	} else {
		$data =  $ci->upload->data();
		return $data;
	}
}

function upload_photo_profil($files)
{
	$ci = &get_instance();

	$name = time() . $files['pic']['name'];
	$nama = $name;
	$config['upload_path']          = FCPATH . 'assets/image/profile_photo/';
	$config['allowed_types']        = 'gif|jpg|png|jpeg';
	$config['max_size']             = 2000;
	$config['max_width']            = 1024;
	$config['max_height']           = 768;
	$config['file_name']            = $nama;

	$ci->upload->initialize($config);
	if (!$ci->upload->do_upload('pic')) {
		return false;
	} else {
		$data =  $ci->upload->data();
		return $data;
	}
}

function upload_font($files)
{
	$ci = &get_instance();

	$name = time() . $files['font']['name'];
	$nama = $name;
	$config['upload_path']          = FCPATH . 'assets/font/';
	$config['allowed_types']        = 'ttf|ttc';
	$config['file_ext']				= '.ttc|.ttf';
	$config['file_name']            = $nama;

	$ci->upload->initialize($config);
	if (!$ci->upload->do_upload('font')) {
		return false;
	} else {
		$data =  $ci->upload->data();
		return $data;
	}
}

function upload_photo_komponen($files)
{
	$ci = &get_instance();

	$name = time() . $files['pic']['name'];
	$nama = $name;
	$config['upload_path'] = FCPATH . 'assets/image/uploads_photo/komponen/';
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	// $config['max_size'] = 2000;
	// $config['max_width'] = 1024;
	// $config['max_height'] = 768;
	$config['file_name'] = $nama;
	$ci->upload->initialize($config);
	if (!$ci->upload->do_upload('pic')) {
		return false;
	} else {
		$data =  $ci->upload->data();
		return $data;
	}
}

function upload_photo_produk($files)
{
	$ci = &get_instance();

	$name = time() . $files['pic']['name'];
	$nama = $name;
	$config['upload_path'] = FCPATH . 'assets/image/uploads_photo/produk/';
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	// $config['max_size'] = 2000;
	// $config['max_width'] = 1024;
	// $config['max_height'] = 768;
	$config['file_name'] = $nama;
	$ci->upload->initialize($config);
	if (!$ci->upload->do_upload('pic')) {
		return false;
	} else {
		$data =  $ci->upload->data();
		return $data;
	}
}

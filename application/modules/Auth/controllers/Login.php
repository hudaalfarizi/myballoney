<?php defined('BASEPATH') or exit('No direct exit allowed');

class Login extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('LoginModel');
	}

	public function index()
	{
		$data['token'] = $this->security->get_csrf_hash();
		$this->load->view('Auth/index', $data);
	}

	public function lupaPassword(Type $var = null)
	{
		$data['token'] = $this->security->get_csrf_hash();
		$this->load->view('Auth/index', $data);
	}

	public function cek_validation()
	{
		$feedback['res'] = '';
		$feedback['url'] = '';
		$feedback['token'] = $this->security->get_csrf_hash();
		$username = htmlspecialchars($this->input->post('username'));
		$password = $this->input->post('password');

		$cek = $this->LoginModel->cek_validation($username, $password);
		// debug($cek);
		if ($cek != NULL) {
			// Ambil data nama role
			$array   = array(
				"id"       => $cek->id,
				"kode" => $cek->kode,
				"nama"  =>	$cek->nama,
				"email"  => $cek->email,
				"no_telp"  => $cek->no_telp,
				"username"  => $cek->username,
				"password"  => $cek->password,
				"apps"		=> "balloney_admin"
			);
			$this->session->set_userdata($array);

			$feedback['res'] = 'success';
			$feedback['url'] = site_url('admin');
		} else {
			$feedback['res'] = 'fail';
			$feedback['url'] = site_url('Auth/Login');
		}

		echo json_encode($feedback);
	}

	public function logout()
	{
	  foreach ($_SESSION as $key => $value) {
	    unset($_SESSION[$key]);
	  }
	  redirect('Auth/Login','refresh');
	}
}

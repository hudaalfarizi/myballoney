<?php defined('BASEPATH') or exit('No direct exit allowed');

class Authpengguna extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('AuthpenggunaModel');
	}

	public function lupaPassword(Type $var = null)
	{
		$data['token'] = $this->security->get_csrf_hash();
		$this->load->view('Auth/index', $data);
	}

	/**
	 *  CRUD Menu Pengguna
	 */
	public function simpan_pengguna()
	{
		$simpan = $this->AuthpenggunaModel->simpan();
		if ($simpan == TRUE) {
			$data['status'] = 'ok';
			$data['msg'] = 'Data berhasil ditambahkan';
			$data['url'] = site_url('user');
			$data['res'] = 'success';
		} else {
			$data['status'] = 'nok';
			$data['msg'] = 'Data gagal ditambahkan';
			$data['url'] = site_url('user');
			$data['res'] = 'gagal';
		}
		echo json_encode($data);
	}

	public function cek_validation()
	{
		$feedback['res'] = '';
		$feedback['url'] = '';
		$feedback['token'] = $this->security->get_csrf_hash();
		$username = htmlspecialchars($this->input->post('username'));
		$password = htmlspecialchars($this->input->post('password'));

		$cek = $this->AuthpenggunaModel->cek_validation($username, $password);

		if ($cek != NULL) {
			// Ambil data nama role
			$data   = array(
				"id"       => $cek->id,
				"pengguna_id" => $cek->pengguna_id,
				"username"  => $cek->username,
				"password"  => $cek->password,
				"apps" => "balloney_user"
			);

			$this->session->set_userdata($data);

			$feedback['status'] = 'ok';
			$feedback['msg'] = 'Berhasil Login';
			$feedback['res'] = 'success';
			$feedback['url'] = site_url('user');
		} else {
			$feedback['status'] = 'nok';
			$feedback['msg'] = 'Username Atau Password Salah';
			$feedback['res'] = 'fail';
			$feedback['url'] = site_url('user');
		}

		echo json_encode($feedback);
	}

	public function do_logout()
	{
		foreach ($_SESSION as $key => $value) {
			unset($_SESSION[$key]);
		}
		$data['status'] = 'ok';
		$data['msg'] = 'Data berhasil ditambahkan';
		$data['url'] = site_url('user');
		$data['res'] = 'success';
		echo json_encode($data);
	}
}

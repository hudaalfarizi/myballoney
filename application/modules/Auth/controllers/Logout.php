<?php defined('BASEPATH') or exit('No direct exit allowed');

class Logout extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function do_logout()
	{
		foreach ($_SESSION as $key => $value) {
			unset($_SESSION[$key]);
		}
		redirect('auth/login', 'refresh');
	}
}

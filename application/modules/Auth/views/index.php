<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Ballooney Service App</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- icheck bootstrap -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/dist/css/adminlte.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/internal/css/modal.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/internal/css/loading.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/internal/css/style.css">

</head>

<body class="hold-transition login-page">


	<div class="row w-800">
		<div class="col-md-6 no-padding">
			<div class="login-box w100">
				<!-- /Login Card Left Side -->
				<div class="card card-left" style="background-image: url('<?= base_url() ?>assets/image/asset/login.jpg'); background-size: cover; padding: 20px !important; background-position: center;">
					<img class="card-img-left" src="<?= base_url(); ?>assets/image/asset/logo.png">
					<div class="title">
						<div style="font-weight: lighter; font-size: 20px; color: white; margin-top: 6px;">Welcome to</div>
						<div style="font-weight: lighter; font-size: 20px;color: white;">Services Management Apps</div>
						<div style="font-weight: lighter; font-size: 20px; color: white; margin-top: 20px;">Services to others, Lead to
							greatness</div>
					</div>
				</div>
			</div>
		</div>

		<!-- /Login Card Right Side -->
		<div class="col-md-6 no-padding">
			<div class="login-box w100">
				<div class="card" style="height: 400px; margin-bottom: 0px !important;">

					<form id="form1">
					<div class="card-body login-card-body" style="border-radius: 100px !important;">
						<h3 class="text-left bold" style="margin-bottom: 10px !important;">LOGIN</h3>
						<small class="label-login-head-login">Enter your username and password to be able to log in to the Ballooney Business Management System</small>
						<div class="label top-10">
							<div class="label-login">Enter Your Username to Login to the application</div>
							<div class="input-group">
								<input type="text" class="form-control-login" name="username" placeholder="Username" autocomplete="off">
								<div class="input-group-append no-border">
									<div class="input-group-text border-bottom">
										<span class="fas fa-user"></span>
									</div>
								</div>
							</div>
						</div>
						<div class="label top-10">
							<div class="label-login">Enter Your Password to Login to the application</div>
							<div class="input-group mb-3">
								<input type="password" class="form-control-login" name="password" placeholder="Password" autocomplete="off">
								<div class="input-group-append no-border">
									<div class="input-group-text border-bottom">
										<span class="fas fa-lock"></span>
									</div>
								</div>
							</div>
						</div>

						<a class="text-info pull-right" href="<?= base_url('auth/lupaPassword'); ?>">I Forgot My
							Password</a><br><br>
						<!-- <a href="<?= base_url('admin') ?>"> -->
						<button type="button" class="btn btn-danger pull-right rounded w-100" onclick="login()">Sign In</button>
						<!-- </a> -->
					</div>
					</form>
					<script>
						$("#iya").on("click", function() {
							$('.modal-notif').hide();
						})
					</script>
				</div>
			</div>
		</div>
	</div>

	<!-- jQuery -->
	<script src="<?= base_url() ?>assets/plugins/jquery/jquery.min.js"></script>
	<!-- Bootstrap 4 -->
	<script src="<?= base_url() ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url() ?>assets/dist/js/adminlte.min.js"></script>
	<!-- Sweet alert 2 -->
	<script src="<?= base_url();?>assets/plugins/sweetalert2/sweetalert2.all.min.js"></script>

	<script>
		function login()
        {
            $.ajax({
                url			: '<?= site_url('Auth/Login/cek_validation');?>',
				data 		: $('#form1').serialize(),
				dataType	: 'JSON',
				type 		: 'POST',
				success: function(response)
				{
					if(response.res == "success")
					{
						Swal.fire({
							icon: 'success',
							title: 'Selamat !',
							text: 'Anda berhasil Login!'
							});
						window.location.href = response.url;
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Ups !',
							text: 'Anda gagal Login!'
							});
						$("#csrf").val(response.token);
					}
					
				}
            })
        }
	</script>
</body>

</html>

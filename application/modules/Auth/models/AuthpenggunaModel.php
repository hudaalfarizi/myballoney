<?php

class AuthpenggunaModel extends CI_Model
{
	public function simpan()
	{
		$result = $this->db->get('public.pengguna');
		$getRow = $this->db->get('public.pengguna')->num_rows();
		$no;
		if($result->num_rows() < 1)
		{
			$no = 1;
		} else {
			$no     = $result->id;
		}
		// $noUrut = $no + 1;
		$char = 'CID';
		$no_code = $getRow + 1;
		$code = $char . sprintf("%03s", $no_code);
		$post = $this->input->post(null, true);

		$result_2 = $this->db->get_where('public.pengguna_akun', ['username' => $post['username']])->row();
		if ($result_2 == TRUE) {
			return false;
		}

		$this->db->trans_begin();
		$data = [
			'kode' => $code,
			'nama' => $post['name'],
			'email' => $post['email'],
			'no_telp' => $post['phone'],
			'tanggal_lahir' => $post['tgl_lahir'],
			'alamat' => $post['city'],
			'tanggal_pendaftaran' => date('Y-m-d'),
			'jenis_kelamin' => '',
			'photo' => 'photo_default.png'
		];

		if ($data['nama'] == "" || $data['email'] == "" || $data['no_telp'] == "" || $data['tanggal_lahir'] == "" || $data['alamat'] == "") {
			return false;
		}

		$this->db->insert('public.pengguna', $data);

		$last_id = $this->db->insert_id();
		$data_2 = [
			'pengguna_id' => $last_id,
			'username' => $post['username'],
			'password' => $post['password']
		];
		$this->db->insert('public.pengguna_akun', $data_2);
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
	}

	function cek_validation($username, $password)
	{
		$getuser = $this->db->get_where('public.pengguna_akun', array('username' => $username));

		if ($getuser->num_rows() > 0) {
			$result = $getuser->row();
			if ($result->password != $password) {
				return FALSE;
			} else {
				return $result;
			}
		} else {
			return FALSE;
		}
	}
}

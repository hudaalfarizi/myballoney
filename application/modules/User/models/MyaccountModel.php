<?php

class MyaccountModel extends CI_Model
{
	public function get_data()
	{
		$id = $this->session->pengguna_id;
		$data = $this->db->get_where('public.pengguna', ['id' => $id]);
		return $data->row();
	}

	public function get_data_2()
	{
		$id = $this->session->pengguna_id;
		$data = $this->db->get_where('public.pengguna_akun', ['pengguna_id' => $id]);
		return $data->row();
	}

	function save_foto($data)
	{
		$data = array('photo' => $data);
		$this->db->where('id', $this->session->pengguna_id);
		$this->db->update('public.pengguna', $data);
	}

	function update_member($table, $data)
	{
		$this->db->where('id', $data['id']);
		return $this->db->update($table, $data);
	}

	function update_member_2($table, $data)
	{
		$this->db->where('pengguna_id', $data['pengguna_id']);
		return $this->db->update($table, $data);
	}
}

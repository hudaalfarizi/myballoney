<?php

class DashboardModel extends CI_Model
{
	public function get_data_About($table)
	{
		$query =  $this->db->get_where($table, ['name' => 'ABOUT US']);
		return $query->row();
	}

	public function get_data_Howtoorder($table)
	{
		$query =  $this->db->get_where($table, ['name' => 'HOW TO ORDER']);
		return $query->row();
	}

	public function get_data_Contactus($table)
	{
		$query =  $this->db->get($table);
		return $query->row();
	}

	public function get_data_Halamancontact($table)
	{
		$query =  $this->db->get_where($table, ['name' => 'CONTACT US']);
		return $query->row();
	}

	public function get_data_Gallery($table)
	{
		$query =  $this->db->get_where($table, ['name' => 'GALLERY']);
		return $query->row();
	}

	public function simpan_pengguna($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function getData($table, $where)
	{
		return $this->db->get_where($table, $where);
	}
	public function get_data_produk()
	{
		$q = $this->db->select('pr.*,tema.nama as nama_tema')
					  ->from('produk.produk as pr')
					  ->where_not_in('pr.status_temp', 1)
					  ->join('produk.tema','tema.id=pr.tema_id','left')
					  ->get();
		return $q;
	}
}

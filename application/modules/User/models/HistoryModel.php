<?php

class HistoryModel extends CI_Model
{
	public function get_data()
	{
		$id = $this->session->pengguna_id;
		$data = $this->db->get_where('public.pengguna', ['id' => $id]);
		return $data->row();
	}

	public function get_data_2()
	{
		$id = $this->session->pengguna_id;
		$data = $this->db->get_where('public.pengguna_akun', ['pengguna_id' => $id]);
		return $data->row();
	}
}

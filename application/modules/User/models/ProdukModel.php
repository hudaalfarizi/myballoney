<?php

class ProdukModel extends CI_Model
{

	public function get_produk_detail($id)
	{
		$q = $this->db->select('
							pr.*,
							k.nama_komponen,
							kk.nama as nama_kk
					  ')
					  ->from('produk.produk pr')
					  ->where('pr.id',$id)
					  ->join('produk.produk_komponen pk','pk.produk_id=pr.id','left')
					  ->join('produk.produk_komponen_detail pkd','pkd.produk_id=pr.id','left')
					  ->join('produk.komponen k','k.id_komponen=pkd.komponen_id','left')
					  ->join('produk.komponen_kategori kk','kk.id=pkd.komponen_kategori_id','left')
					  ->join('produk.tema t','t.id=pr.tema_id','left')
					  ->get();
		return $q->row();
	}

	public function get_data_addt($id)
	{
		$q = $this->db->select('
							pt.tambahan_id,
							pt.harga_jual,
							t.kode,
							t.name,
					  ')
					  ->from('produk.produk_tambahan pt')
					  ->where('pt.produk_id',$id)
					  ->join('produk.tambahan t','t.id=pt.tambahan_id','left')
					  ->get();
		return $q->result();
	}

	public function get_data_komponen($id)
	{
		$q = $this->db->select('
							DISTINCT(kk.nama),
							k.nama_komponen
					   ')
					  ->from('produk.produk_komponen pk')
					  ->where('pk.produk_id',$id)
					  ->join('produk.komponen_kategori kk','kk.id=pk.komponen_kategori_id','left')
					  ->join('produk.produk_komponen_detail pkd','pkd.produk_id=pk.produk_id','left')
					  ->join('produk.komponen k','k.id_komponen=pkd.komponen_id')
					  ->get();
		return $q->result();
	}
}

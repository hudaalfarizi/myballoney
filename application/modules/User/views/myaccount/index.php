<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url() ?>assets/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init"></label>
				<img src="<?= base_url() . "assets/image/profile_photo/" . $akun->photo; ?>" style="height: 100%;">
			</div>
			<label class="account-name"><?= $akun->nama; ?> / Member</label>
		</div>

		<a href="<?= base_url('user') ?>">
			<div class="rounded c-base pull-right store">
				<div class="account-img c-trans rounded">
					<img src="<?= base_url() ?>assets/image/asset/monitor red.png" style="background-size: cover; background-repeat: no-repeat; padding: 5px;">
				</div>
				<label class="account-name">STORE</label>
			</div>
		</a>

	</div>

	<div class="content-container">
		<div class="col col-content padding-content">
			<h4 class="bold">MY ACCOUNT</h4>
			<div class="date-info f-red padding-tanggal running-date"></div><br />

			<div class="flex">
				<div class="f-col-4 f-float-round pad-sm" style="margin-top: 5px;">
					<div>
						<img src="<?= base_url() . "assets/image/profile_photo/" . $akun->photo; ?>" style="background-size: cover; background-repeat: no-repeat; width: 100%;">
					</div>
				</div>

				<div class="f-col-9 pad-sm" style="margin-left: 50px;">
					<div class="flex" style="padding-bottom: 0px !important; margin-bottom: 0px !important;">
						<div class="f-col">
							<div class="label">
								<h3 class="bold"><?= $akun->nama; ?></h3>
								<div class="coral bold"><?= $akun->kode ?> | <?= $akun->no_telp ?></div>
							</div>
						</div>
					</div>
					<div class="flex mt-3">
						<div class="f-col-9">
							<div id="loading" style="display:none;">...loading</div>
							<div class="bold">Upload Foto Pengguna</div>
							<input type="file" id="pic" name="pic" style="display:none" onchange="document.getElementById('filename').value=this.value" accept="image/*">
							<input type="text" class="form-control" id="filename" placeholder="Pilih File Foto Anda" onclick="document.getElementById('pic').click()">
						</div>
						<div class="f-col-3" style="margin-top: 25px;">
							<button onclick="uploadfoto()" id="foto" name="foto" type="button" class="btn btn-sm btn-primary w-100">UPLOAD</button>
						</div>
					</div>
				</div>
			</div>
			<br>

			<div class="flex">
				<div class="f-col-9 border-bottom">
					<div class="head-form-control darkred bold">MEMBER DATA</div>
				</div>
			</div>

			<div class="flex pad-xs" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;margin-bottom: 10px;">
				<div class="f-col">
					<div id="k1" class="bold">Personal Data</div>
				</div>
			</div>

			<div class="flex pad-sm" style="margin-bottom: 13px;">
				<div class="f-col-9">
					<form id="editdata">
						<input type="hidden" name="id" id="id" class="form-control" value="<?= $akun->id ?>">
						<div class="label">
							<div class="head-form-control-2">Nama</div>
							<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama" value="<?= $akun->nama ?>">
						</div>
						<div class="label">
							<div class="head-form-control-2">No. Tlp</div>
							<input type="number" name="phone" id="phone" class="form-control" placeholder="Input Tlp." value="<?= $akun->no_telp ?>">
						</div>
						<div class="label">
							<div class="head-form-control-2">Email Pengguna</div>
							<input type="email" name="email" id="email" class="form-control" placeholder="Input Email Pengguna" value="<?= $akun->email ?>">
						</div>
						<div class="label">
							<div class="head-form-control-2">Gender</div>
							<select name="gender" id="gender" class="form-control">
								<option selected disabled>Pilih Gender</option>
								<option value="Laki-Laki">Laki-laki</option>
								<option value="Perempuan">Perempuan</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Tangal Lahir</div>
							<!-- <i class="far fa-calendar-alt icon-tgl"></i> -->
							<input type="date" name="tgl_lahir" id="tgl_lahir" placeholder="tanggal" class="form-control" value="<?= $akun->tanggal_lahir ?>">
						</div>
						<!-- <div class="label">
						<div class="head-form-control-2">City / Kota Tempat Tinggal</div>
						<input type="text" name="city" id="city" class="form-control" placeholder="Input Tempat Tinggal">
					</div> -->
						<div class="label">
							<div class="head-form-control-2">Alamat</div>
							<input type="text" id="alamat" name="alamat" class="form-control" placeholder="Input Alamat">
						</div>
						<div class="label">
							<div class="head-form-control-2">Bergabung Sejak</div>
							<!-- <i class="far fa-calendar-alt icon-tgl"></i> -->
							<input type="text" readonly name="tgl_bergabung" id="tgl_bergabung" class="form-control" placeholder="22-april-2020 ( Auto ) " value="<?= $akun->tanggal_pendaftaran ?>">
						</div>
						<div class="label">
							<div class="head-form-control-2">Username</div>
							<input type="text" readonly name="username" id="username" class="form-control" placeholder="Input Username" value="<?= $akun_2->username ?>">
						</div>
						<div class="label">
							<div class="head-form-control-2">Password</div>
							<input type="password" name="password" id="password" class="form-control" placeholder="Input Password">
						</div>
						<button id="submit" name="submit" type="submit" onclick="editdata()" class="btn c-jingga btn-lg right mt-2">EDIT</bu>
					</form>
				</div>
			</div>

		</div>

		<div class="col col-menu pad-sm">
			<div class="menu-logo">
				<img src="<?= base_url() ?>assets/image/asset/logo.png" alt="">
			</div>
			<!-- <div class="menu-button">
				<a href="<?= base_url('shopcart') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Red Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">SHOPPING CHART</div>
						<div class="menu-desc">Daftar Pembelian Produk Yang Anda Pilih</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('transaction') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Icon Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">TRANSACTION</div>
						<div class="menu-desc">Melihat Seluruh Aktifitas Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('history') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/photo_default.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">HISTORY</div>
						<div class="menu-desc">Riwayat Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div> -->
			<div class="menu-button">
				<a href="<?= base_url('myAccount/') . $this->session->pengguna_id; ?>" class="menu-item active">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/document blue.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">MY ACCOUNT</div>
						<div class="menu-desc">Setting Akun Untuk Privasi Dan Kenyamanan Dalam Bertransaksi</div>
					</div>
				</a>
			</div>
		</div>
	</div>

	<script>
		function uploadfoto() {
			formdata = new FormData();
			formdata.append('pic', $('#pic').prop('files')[0]);
			$('#loading').show();
			$.ajax({
				url: '<?php echo site_url('User/Myaccount/updatephoto'); ?>',
				data: formdata,
				type: "POST",
				dataType: 'JSON',
				contentType: false,
				cache: false,
				processData: false,
				success: function(response) {
					$('#loading').hide();
					if (response.status == 'ok') {
						Swal.fire({
							icon: 'success',
							title: 'Selamat !',
							text: 'Anda berhasil Upload!'
						});
						location.reload();
					} else if (response.status == 'nok') {
						Swal.fire({
							icon: 'error',
							title: 'Ups !',
							text: 'Anda gagal Upload!'
						});
						location.reload();
					}
				}
			})
		}

		function editdata() {
			// var pengguna_id = $('#id').val();
			// var nama = $('#nama').val();
			// var phone = $('#phone').val();
			// var email = $('#email').val();
			// var gender = $('#gender').val();
			// var tgl_lahir = $('#tgl_lahir').val();
			// var city = $('#city').val();
			// var alamat = $('#alamat').val();
			// var tgl_bergabung = $('#tgl_bergabung').val();
			// var username = $('#username').val();
			// var password = $('#password').val();
			if ($("#password").val() == "") {
				alert('password harus di isi');
				exit();
			}
			$.ajax({
				url: '<?= site_url('User/Myaccount/simpan_edit_member'); ?>',
				data: $('#editdata').serialize(),
				// pengguna_id: pengguna_id,
				// nama: nama,
				// phone: phone,
				// email: email,
				// gender: gender,
				// tgl_lahir: tgl_lahir,
				// tgl_bergabung: tgl_bergabung,
				// city: city,
				// alamat: alamat,
				// username: username,
				// password: password
				type: 'POST',
				dataType: 'JSON',
				success: function(response) {
					if (response.status == 'success') {
						Swal.fire({
							icon: 'success',
							title: 'Selamat !',
							text: 'Anda berhasil edit data!'
						});
						location.reload();
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Ups !',
							text: 'Anda gagal edit data!'
						});
						location.reload();
					}
				}
			});
		}
	</script>

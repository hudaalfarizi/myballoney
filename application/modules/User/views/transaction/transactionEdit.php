<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url() ?>assets/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init"></label>
				<img src="<?= base_url() . "assets/image/profile_photo/" . $akun->photo; ?>" style="height: 100%;">
			</div>
			<label class="account-name"><?= $akun->nama; ?> / Member</label>
		</div>
		<a href="<?= base_url('user') ?>">
			<div class="rounded c-base pull-right store">
				<div class="account-img c-trans rounded">
					<img src="<?= base_url() ?>assets/image/asset/monitor red.png" style="background-size: cover; background-repeat: no-repeat; padding: 5px;">
				</div>
				<label class="account-name">STORE</label>
			</div>
		</a>
	</div>

	<div class="content-container">
		<div class="col col-content padding-content">
			<h4 class="bold">EDIT PRODUK</h4>
			<div class="date-info padding-tanggal">
				<a href="<?= base_url('transaction/transactionMenunggu') ?>" class="btn btn-danger rounded pull-right">X</a>
			</div>

			<div class="flex">
				<div class="f-col-5 f-float-round pad-sm" style="margin-top: 5px;">
					<div>
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="" data-zoom-image="<?= base_url() ?>assets/image/asset/Red Baloon Large.png" id="zoom">
					</div>
				</div>
				<div class="f-col-6 pad-sm">
					<div class="f-col f-float-round pad-sm">
						<div class="flex" style="padding-bottom: 0px !important; margin-bottom: 0px !important;">
							<div class="f-col">
								<div class="label">
									<h3 class="bold">Big Balloon</h3>
									<div class="coral bold">BF_123456789</div>
									<div class="label-4desc">RP. 250.000,-</div>
								</div>
							</div>
						</div>
						<div class="flex">
							<div class="f-col-9">
								<div class="label">
									<h5 class="label-4">Tema</h5>
									<div class="coral bold">Birthday</div>
								</div>
							</div>
							<div class="f-col-4">
								<div class="label">
									<h5 class="label-4">Status</h5>
									<div class="coral bold">Top Product</div>
								</div>
							</div>
						</div>
					</div>

					<div class="flex pad-xs" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;margin-bottom: 10px;">
						<div class="f-col">
							<div id="k1" class="bold">Keterangan</div>
						</div>
					</div>

					<div class="flex f-float-round pad-sm">
						<div class="f-col-9">
							<div class="label">
								<h5 class="bold">Keterangan Tentang Produk :</h5>
								<ol>
									<li>Ukuran 1 x 1"</li>
									<li>dst</li>
									<li>dst</li>
									<li>dst</li>
									<li>dst</li>
								</ol>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="flex pad-xs f-float-round" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;">
				<div class="f-col">
					<div id="k1" class="bold">Additional Options</div>
				</div>
			</div>

			<div class="flex f-float-round pad-sm mb-3">
				<div class="f-col">
					<div class="flex">
						<div class="flex f-float-round" style="background-color: #0482e3;">
							<div class="f-col-9 mt-3 ml-5">
								<div class="label">
									<select class="form-control" name="produk" id="produk">
										<option selected>Pilih Produk Additional Options</option>
									</select>
								</div>
							</div>
							<div class="f-col-3 mt-3">
								<div class="label">
									<input type="text" name="qty" id="qty" placeholder="Quantity" class="form-control">
								</div>
							</div>
							<div class="f-col-3 mt-3">
								<button id="submit" name="submit" class="btn btn-warning" style="color: white;">TAMBAH</button>
							</div>
						</div>
					</div>
					<div class="flex">
						<table class="table table-borderless" style="text-align: center;">
							<tbody>
								<tr>
									<td>1</td>
									<td>001</td>
									<td>Ballon Hijau</td>
									<td>13</td>
								</tr>
								<tr>
									<td>2</td>
									<td>002</td>
									<td>Balloon Merah</td>
									<td>13</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<div class="flex pad-xs" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;margin-bottom: 10px;">
				<div class="f-col">
					<div class="bold">Komponen Produk</div>
				</div>
			</div>

			<div class="komponen">
				<div class="flex pad-xs f-float-round" style="background-color: #0482e3; color:white; border-radius: 5px; margin-top: 5px;">
					<div class="f-col">
						<div id="k1" class="bold">Big Ballon</div>
					</div>
				</div>

				<div class="flex f-float-round pad-sm mb-3">
					<div class="flex f-bordered">
						<div class="f-col" style="margin-top: 5px;">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" style="width: 150px; margin-top: 50px;">
							</div>
							<label class="account-init" style="margin-left: 25%;font-size: 14px;">Red Balloon</label>
						</div>
						<div class="f-col" style="margin-top: 5px;">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" style="width: 150px; margin-top: 50px;">
							</div>
							<label class="account-init" style="margin-left: 25%;font-size: 14px;">Red Balloon</label>
						</div>
						<div class="f-col" style="margin-top: 5px;">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" style="width: 150px; margin-top: 50px;">
							</div>
							<label class="account-init" style="margin-left: 25%;font-size: 14px;">Red Balloon</label>
						</div>
					</div>

					<div class="f-col-10" style="margin-left: 20px;">
						<div class="flex" style="padding-bottom: 0px !important; margin-bottom: 0px !important;">
							<div class="f-col">
								<div class="label">
									<h5 class="label">Pilihan Komponen</h5>
								</div>
								<textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="- Red"></textarea>
							</div>
						</div>
						<div class="f-col">
							<div class="label">
								<h5 class="label-4">Wording Big Balloon</h5>
							</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Font</option>
								<option value="">Arial</option>
								<option value="">Roboto</option>
							</select>
							<input type="text" class="form-control" placeholder="Tulis" style="margin-top: 15px">
						</div>
					</div>
				</div>

				<div class="flex pad-xs f-float-round" style="background-color: #0482e3; color:white; border-radius: 5px; margin-top: 5px;">
					<div class="f-col">
						<div id="k1" class="bold">Medium Ballon</div>
					</div>
				</div>

				<div class="flex f-float-round pad-xs mb-3">
					<div class="flex f-bordered">
						<div class="f-col-3" style="margin-top: 5px;">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" style="width: 150px; margin-top: 50px;">
							</div>
							<label class="account-init" style="margin-left: 25%;font-size: 14px;">Red Balloon</label>
						</div>
						<div class="f-col-3" style="margin-top: 5px;">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" style="width: 150px; margin-top: 50px;">
							</div>
							<label class="account-init" style="margin-left: 25%;font-size: 14px;">Red Balloon</label>
						</div>
						<div class="f-col-3" style="margin-top: 5px;">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" style="width: 150px; margin-top: 50px;">
							</div>
							<label class="account-init" style="margin-left: 25%;font-size: 14px;">Red Balloon</label>
						</div>
					</div>
					<div class="f-col-10" style="margin-left: 20px;">
						<div class="flex" style="padding-bottom: 0px !important; margin-bottom: 0px !important;">
							<div class="f-col">
								<div class="label">
									<h5 class="label">Pilihan Komponen</h5>
								</div>
								<textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="- Red"></textarea>
							</div>
						</div>
						<div class="f-col">
							<div class="label">
								<h5 class="label-4">Wording Big Balloon</h5>
							</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Font</option>
								<option value="">Arial</option>
								<option value="">Roboto</option>
							</select>
							<input type="text" class="form-control" placeholder="Tulis" style="margin-top: 15px">
						</div>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col">
					<div class="flex pad-xs f-float-round" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;">
						<div class="f-col">
							<div id="k1" class="bold">Special Note</div>
						</div>
					</div>
				</div>
				<div class="f-col">
					<div class="flex pad-xs f-float-round" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;">
						<div class="f-col">
							<div id="k1" class="bold">Important Notes</div>
						</div>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col">
					<textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="Tulis Catatan Khusus Untuk Pesanan Anda"></textarea>
				</div>
				<div class="f-col">
					<textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="Tulis Catatan Khusus Untuk Pesanan Anda"></textarea>
				</div>
			</div>

			<div class="f-float-round pad-xs" style="background-color: #0482e3;">
				<div class="flex place" style="margin-left: 60px;">
					<div class="f-col-4">
						<div class="label">
							<div style="font-size: 20px; color: white;" class="bold">JUMLAH PESANAN</div>
							<input style="padding: 20px;" type="text" name="jml_pesanan" id="jml_pesanan" placeholder="1" class="form-control">
						</div>
					</div>
					<div class="f-col-4">
						<div class="label">
							<div style="font-size: 20px; color: white;" class="bold">SUB TOTAL</div>
							<input style="padding: 20px;" type="text" name="qty" id="qty" placeholder="Rp. 250.000" class="form-control">
						</div>
					</div>
					<div class="f-col-4">
						<div class="label" style="padding-top: 30px;">
							<button id="submit" name="submit" class="btn btn-danger" style="color: white; padding: 20px;">SIMPAN</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col col-menu pad-sm">
			<div class="menu-logo">
				<img src="<?= base_url() ?>assets/image/asset/logo.png" alt="">
			</div>
			<div class="menu-button">
				<a href="<?= base_url('shopcart') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Red Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">SHOPPING CHART</div>
						<div class="menu-desc">Daftar Pembelian Produk Yang Anda Pilih</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('transaction') ?>" class="menu-item active">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Icon Transaksi Blue.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">TRANSACTION</div>
						<div class="menu-desc">Melihat Seluruh Aktifitas Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('history') ?>" class="menu-item">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/photo_default.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">HISTORY</div>
						<div class="menu-desc">Riwayat Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<!-- <div class="menu-button">
				<a href="<?= base_url('myAccount') ?>" class="menu-item">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/document red.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">MY ACCOUNT</div>
						<div class="menu-desc">Setting Akun Untuk Privasi Dan Kenyamanan Dalam Bertransaksi</div>
					</div>
				</a>
			</div> -->
		</div>
	</div>

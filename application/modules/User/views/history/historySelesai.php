<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url() ?>assets/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init"></label>
				<img src="<?= base_url() . "assets/image/profile_photo/" . $akun->photo; ?>" style="height: 100%;">
			</div>
			<label class="account-name"><?= $akun->nama; ?> / Member</label>
		</div>
		<a href="<?= base_url('user') ?>">
			<div class="rounded c-base pull-right store">
				<div class="account-img c-trans rounded">
					<img src="<?= base_url() ?>assets/image/asset/monitor red.png" style="background-size: cover; background-repeat: no-repeat; padding: 5px;">
				</div>
				<label class="account-name">STORE</label>
			</div>
		</a>
	</div>
	<div class="content-container">
		<div class="col col-content padding-content">
			<h4 class="bold">DETAIL TRANSAKSI</h4>
			<div class="date-info f-green padding-content">
				<a href="<?= base_url('history') ?>" class="btn btn-danger rounded pull-right">X</a>
			</div><br>
			<div class="flex">
				<div class="f-col-4 f-float-round pad-sm" style="margin-top: 5px;">
					<div class="image-round">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-7 pad-sm" style="margin-left: 50px;">
					<div class="flex" style="padding-bottom: 0px !important; margin-bottom: 0px !important;">
						<div class="f-col">
							<div class="label">
								<h3 class="bold">Red Balloon</h3>
								<div class="coral bold">CID_2342348 | 0821239389</div>
								<div style="font-size: 12px;">ID TRANSAKSI</div>
							</div>
						</div>
					</div>
					<div class="flex">
						<div class="f-col-9">
							<button class="btn btn-sm btn-warning w95 bold" style="padding-left: 2px !important;color: white;">TR_234523757</button>
						</div>
						<!-- <div class="f-col-4">
							<button class="btn btn-sm btn-primary w95">PROFIL</button>
						</div> -->
					</div>
					<div class="flex">
						<div class="f-col-4">
							<div class="label">
								<div style="font-size: 12px;">Total Transaksi</div>
								<button class="btn btn-gray btn-sm bold w95">Rp. 350.000</button>
							</div>
						</div>
						<div class="f-col-4">
							<div style="font-size: 12px;">Total Produk</div>
							<button class="btn btn-gray btn-sm bold w95">32</button>
						</div>
						<div class="f-col-4">
							<div style="font-size: 12px;">Status Transaksi</div>
							<button class="btn btn-gray btn-sm bold w95">SELESAI</button>
						</div>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-bottom: 0px !important; padding-bottom: 0px !important;">
				<div class="f-col-5">
					<div class="head-form-control">DETAIL TRANSAKSI</div>
				</div>
				<div class="f-col-6 bold">
					<div class="coral" style="font-size: 12px;padding-top: 3px !important;">TRANSACTION DATE : SELASA 24
						MARET 2020</div>
				</div>
				<div class="f-col-5 bold" style="font-size: 12px;padding-top: 3px !important;">
					<div class="coral">TRANSACTION TIME : 07:07:07 WIB</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 0px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">Produk Pesanan</div>
				</div>
			</div>

			<div class="flex f-float-round pad-sm ">
				<div class="f-col-3 f-float-round">
					<div class="image-round-sm">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-9">
					<div class="flex border-bottom">
						<div class="f-col-9">
							<div class="label">
								<h5 class="bold">PAKET BALON FOIL</h5>
								<div class="coral bold">BF 123165432</div>
							</div>
						</div>
						<div class="f-col-4">
							<div class="label pull-right">
								<h5 class="label-4">Kode Transaksi Produk</h5>
								<div class="label-4desc">TRP_123165432</div>
							</div>
						</div>
					</div>
					<div class="flex">
						<div class="f-col-2">
							<div class="label">
								<div>Jumlah</div>
								<div class="darkred">1 Paket</div>
							</div>
						</div>
						<div class="f-col-9">
							<div class="label">
								<div>Harga</div>
								<div class="darkred">Rp. 250.000</div>
							</div>
						</div>
						<div class="f-col-3 h-margin-r-0 mt-2">
							<a href="<?= base_url('history/historySelesaiDetail') ?>">
								<button class="btn btn-primary btn-sm pull-right w70">DETAIL</button>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="flex f-float-round pad-sm my-3">
				<div class="f-col-3 f-float-round">
					<div class="image-round-sm">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-9">
					<div class="flex border-bottom">
						<div class="f-col-9">
							<div class="label">
								<h5 class="bold">PAKET BALON FOIL</h5>
								<div class="coral bold">BF 123165432</div>
							</div>
						</div>
						<div class="f-col-4">
							<div class="label pull-right">
								<h5 class="label-4">Kode Transaksi Produk</h5>
								<div class="label-4desc">TRP_123165432</div>
							</div>
						</div>
					</div>
					<div class="flex">
						<div class="f-col-2">
							<div class="label">
								<div>Jumlah</div>
								<div class="darkred">1 Paket</div>
							</div>
						</div>
						<div class="f-col-9">
							<div class="label">
								<div>Harga</div>
								<div class="darkred">Rp. 250.000</div>
							</div>
						</div>
						<div class="f-col-3 h-margin-r-0 mt-2">
							<a href="<?= base_url('history/historySelesaiDetail') ?>">
								<button class="btn btn-primary btn-sm pull-right w70">DETAIL</button>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col pad-sm" style="text-align: center;">
					<button class="more-items">Show More Items <i class="fas fa-sort-down"></i></button>
				</div>
			</div>

			<div class="flex" style="margin-top: 0px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">Pengiriman</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Metode Pengiriman</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Metode Pengiriman</option>
								<option value="" selected>Diambil Sendiri</option>
								<option value="">Dikirim</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Waktu Pengiriman</div>
							<input type="text" class="form-control" placeholder="15 : 23 : 28 WIB">
						</div>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 10px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">Pengiriman</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Metode pengiriman</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Metode Pengiriman</option>
								<option value="">Diambil Sendiri</option>
								<option value="" selected>Dikirim</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Expedisi</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Expedisi</option>
								<option value="">JNE</option>
								<option value="">TIKI</option>
								<option value="">Gojek</option>
								<option value="">Grab</option>
								<option value="">JNT</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Alamat Pengiriman</div>
							<input type="text" class="form-control" placeholder="Tulis Alamat">
						</div>
						<div class="label">
							<div class="head-form-control-2">Kontak Penerima</div>
							<input type="text" class="form-control" placeholder="Tulis Kontak Penerima">
						</div>
						<div class="label">
							<div class="head-form-control-2">Biaya</div>
							<input type="text" class="form-control" placeholder="Rp.">
						</div>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 10px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">TOTAL TRANSAKSI</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Transaksi Produk</div>
							<input type="text" class="form-control" placeholder="Rp 250.000">
						</div>
						<div class="label">
							<div class="head-form-control-2">Biaya Pengiriman</div>
							<input type="text" class="form-control" placeholder="Rp 50.000">
						</div>
						<div class="label">
							<div class="head-form-control-2">Total Transaksi</div>
							<input type="text" class="form-control" placeholder="Rp 350.000">
						</div>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 10px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">KONFIRMASI PEMBAYARAN</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Status Pembayaran</div>
							<select name="" id="" class="form-control">
								<option value="">Paid</option>
								<option value="">Unpaid</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Metode Pemabayaran</div>
							<select name="" id="" class="form-control">
								<option selected value="">Transfer</option>
								<option value="">Tunai</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Nama Bank Customer</div>
							<select name="" id="" class="form-control">
								<option value="">BCA</option>
								<option value="">BNI</option>
								<option value="">MANDIRI</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Nomor Rekening</div>
							<input type="text" class="form-control" placeholder="No. Rek Customer">
						</div>
						<div class="label">
							<div class="head-form-control-2">Jumlah Uang Yang Dibayarkan / Transfer</div>
							<input type="text" class="form-control" placeholder="Rp 350.000">
						</div>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 10px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">KONFIRMASI PEMBAYARAN</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Status Pembayaran</div>
							<select name="" id="" class="form-control">
								<option value="">Paid</option>
								<option value="">Unpaid</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Metode Pemabayaran</div>
							<select name="" id="" class="form-control">
								<option value="">Transfer</option>
								<option selected value="">Tunai</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Jumlah Uang Yang Dibayarkan / Transfer</div>
							<input type="text" class="form-control" placeholder="Rp 350.000">
						</div>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 10px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">PENYELESAIAN ORDER</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Metode Pengiriman</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Metode Pengiriman</option>
								<option selected value="">Diambil Sendiri</option>
								<option value="">Dikirim</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Rencana Waktu Pengambilan</div>
							<input type="text" class="form-control" placeholder="17:31:15 WIB">
						</div>
						<div class="label">
							<div class="head-form-control-2">Waktu Pengambilan</div>
							<input type="text" class="form-control" placeholder="19:15:30 WIB">
						</div>
						<div class="label">
							<div class="head-form-control-2">Nama Penerima</div>
							<input type="text" class="form-control" placeholder="Rahma">
						</div>
						<div class="label">
							<div class="head-form-control-2">No Tlp</div>
							<input type="text" class="form-control" placeholder="0819129823798">
						</div>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 10px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">PENYELESAIAN ORDER</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Metode Pengiriman</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Metode Pengiriman</option>
								<option value="">Diambil Sendiri</option>
								<option selected value="">Dikirim</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Waktu Pengiriman</div>
							<input type="text" class="form-control" placeholder="19:15:30 WIB">
						</div>
						<div class="label">
							<div class="head-form-control-2">No Bukti Pengiriman</div>
							<input type="text" class="form-control" placeholder="12334234">
						</div>
						<div class="label">
							<div class="head-form-control-2">Kontak Pengirim</div>
							<input type="text" class="form-control" placeholder="0819129823798">
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col col-menu pad-sm">
			<div class="menu-logo">
				<img src="<?= base_url() ?>assets/image/asset/logo.png" alt="">
			</div>
			<div class="menu-button">
				<a href="<?= base_url('shopcart') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Red Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">SHOPPING CHART</div>
						<div class="menu-desc">Daftar Pembelian Produk Yang Anda Pilih</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('transaction') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Icon Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">TRANSACTION</div>
						<div class="menu-desc">Melihat Seluruh Aktifitas Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('history') ?>" class="menu-item active">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/photo_default.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">HISTORY</div>
						<div class="menu-desc">Riwayat Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<!-- <div class="menu-button">
				<a href="<?= base_url('myAccount') ?>" class="menu-item">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/document red.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">MY ACCOUNT</div>
						<div class="menu-desc">Setting Akun Untuk Privasi Dan Kenyamanan Dalam Bertransaksi</div>
					</div>
				</a>
			</div> -->
		</div>
	</div>

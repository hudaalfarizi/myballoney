<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url() ?>assets/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init"></label>
				<img src="<?= base_url() . "assets/image/profile_photo/" . $akun->photo; ?>" style="height: 100%;">
			</div>
			<label class="account-name"><?= $akun->nama; ?> / Member</label>
		</div>

		<a href="<?= base_url('user') ?>">
			<div class="rounded c-base pull-right store">
				<div class="account-img c-trans rounded">
					<img src="<?= base_url() ?>assets/image/asset/monitor red.png" style="background-size: cover; background-repeat: no-repeat; padding: 5px;">
				</div>
				<label class="account-name">STORE</label>
			</div>
		</a>

	</div>

	<div class="content-container">
		<div class="col col-content padding-content">
			<h4 class="bold">HISTORY</h4>
			<div class="date-info f-red padding-tanggal running-date"></div><br />

			<div class="flex">
				<div class="f-col-5 input-icon-tgl">
					<div style="font-size: 14px;">Tanggal Awal</div>
					<i class="far fa-calendar-alt icon-tgl"></i>
					<input type="date" placeholder="tanggal" class="form-control" style="padding-left: 30px;">
				</div>
				<div class="f-col-5 input-icon-tgl">
					<div style="font-size: 14px;">Tanggal Akhir</div>
					<i class="far fa-calendar-alt icon-tgl"></i>
					<input type="date" placeholder="tanggal" class="form-control" style="padding-left: 30px;">
				</div>
			</div>
			<div class="flex">
				<div class="f-col-12 input-icon">
					<div style="font-size: 14px;">Cari Transaksi</div>
					<select name="" id="" class="form-control">
						<option value="" selected="disabled">Cari Transaksi</option>
						<option value="">ID Transaksi</option>
					</select>
				</div>
				<div class="f-col-2 input-icon">
					<button class="btn btn-primary pull-right" style="margin-top: 20px;width: 120px;">CARI</button>
				</div>
			</div>
			<br>
			<div class="flex">
				<div class="f-col-9 border-bottom">
					<div class="head-form-control darkred bold">Aktifitas Transaksi Tanggal 25 Maret 2020</div>
				</div>
			</div>
			<div class="flex f-float-round pad-sm" style="margin-bottom: 13px;">
				<div class="f-col-2">
					<div class="image-round-sm">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-9">
					<div class="flex" style="border-bottom: 2px solid #ddd; padding-bottom: 2px !important;">
						<div class="f-col-8">
							<div class="label">
								<h5 class="bold">Red Balloon</h5>
								<div class="coral" style="margin-top: 3px; font-size: 15px;">CID_123 | 089099898</div>
							</div>
						</div>
						<div class="f-col-4 pad-sm">
							<a href="<?= base_url('history/historySelesai') ?>" class="btn btn-sm btn-success pull-right w-75">SELESAI</a>
						</div>
					</div>
					<div class="flex" style="margin-top: 3px;;">
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">ID Transaksi</div>
								<div class="coral" style=" font-size: 12px;">CID_123 | 089099898</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Total Transaksi</div>
								<div class="coral" style="font-size: 12px;">Rp 150.000</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Tanggal Transaksi</div>
								<div class="coral" style="font-size: 12px;">Selasa, 25 MAret 2020</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Waktu Transaksi</div>
								<div class="coral" style="font-size: 12px;">07:08:15 WIB</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="flex f-float-round pad-sm" style="margin-bottom: 13px;">
				<div class="f-col-2">
					<div class="image-round-sm">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-9">
					<div class="flex" style="border-bottom: 2px solid #ddd; padding-bottom: 2px !important;">
						<div class="f-col-8">
							<div class="label">
								<h5 class="bold">Red Balloon</h5>
								<div class="coral" style="margin-top: 3px; font-size: 15px;">CID_123 | 089099898</div>
							</div>
						</div>
						<div class="f-col-4 pad-sm">
							<a href="#" class="btn btn-sm btn-danger pull-right w-75">CANCEL</a>
						</div>
					</div>
					<div class="flex" style="margin-top: 3px;;">
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">ID Transaksi</div>
								<div class="coral" style=" font-size: 12px;">CID_123 | 089099898</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Total Transaksi</div>
								<div class="coral" style="font-size: 12px;">Rp 150.000</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Tanggal Transaksi</div>
								<div class="coral" style="font-size: 12px;">Selasa, 25 MAret 2020</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Waktu Transaksi</div>
								<div class="coral" style="font-size: 12px;">07:08:15 WIB</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="flex f-float-round pad-sm" style="margin-bottom: 13px;">
				<div class="f-col-2">
					<div class="image-round-sm">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-9">
					<div class="flex" style="border-bottom: 2px solid #ddd; padding-bottom: 2px !important;">
						<div class="f-col-8">
							<div class="label">
								<h5 class="bold">Red Balloon</h5>
								<div class="coral" style="margin-top: 3px; font-size: 15px;">CID_123 | 089099898</div>
							</div>
						</div>
						<div class="f-col-4 pad-sm">
							<a href="#" class="btn btn-sm btn-success pull-right w-75">SELESAI</a>
						</div>
					</div>
					<div class="flex" style="margin-top: 3px;;">
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">ID Transaksi</div>
								<div class="coral" style=" font-size: 12px;">CID_123 | 089099898</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Total Transaksi</div>
								<div class="coral" style="font-size: 12px;">Rp 150.000</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Tanggal Transaksi</div>
								<div class="coral" style="font-size: 12px;">Selasa, 25 MAret 2020</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div style="font-size: 14px;">Waktu Transaksi</div>
								<div class="coral" style="font-size: 12px;">07:08:15 WIB</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col pad-sm" style="text-align: center;">
					<button class="more-items">Show More Items <i class="fas fa-sort-down"></i></button>
				</div>
			</div>
		</div>

		<div class="col col-menu pad-sm">
			<div class="menu-logo">
				<img src="<?= base_url() ?>assets/image/asset/logo.png" alt="">
			</div>
			<div class="menu-button">
				<a href="<?= base_url('shopcart') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Red Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">SHOPPING CHART</div>
						<div class="menu-desc">Daftar Pembelian Produk Yang Anda Pilih</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('transaction') ?>" class="menu-item ">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Icon Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">TRANSACTION</div>
						<div class="menu-desc">Melihat Seluruh Aktifitas Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('history') ?>" class="menu-item active">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/photo_default.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">HISTORY</div>
						<div class="menu-desc">Riwayat Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<!-- <div class="menu-button">
				<a href="<?= base_url('myAccount') ?>" class="menu-item">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/document red.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">MY ACCOUNT</div>
						<div class="menu-desc">Setting Akun Untuk Privasi Dan Kenyamanan Dalam Bertransaksi</div>
					</div>
				</a>
			</div> -->
		</div>
	</div>

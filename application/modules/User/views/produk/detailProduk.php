<body>
	<form class="form-construct">
		<input type="hidden" name="idproduk" value='<?= $produk->id;?>'>
		<input type="hidden" name="namaproduk" value='<?= $produk->nama;?>'>
		<input type="hidden" name="hargajual" value='<?= $produk->harga_jual;?>'>
	</form>
	<!-- Navigation -->
	<div class="header-product" style="background-image: url('<?= base_url() ?>assets/image/asset/bg2.jpg'); background-size: cover; background-repeat: no-repeat;">
		<nav class="navbar navbar-expand-lg navbar-dark navbar-ballooney static-top">
			<a class="navbar-brand" href="<?= base_url('user') ?>" style="margin-left: 20px;">
				<img src="<?= base_url() ?>assets/image/asset/logo.png">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-auto">
					<li class="nav-item active">
						<a class="nav-link" href="<?= base_url('user/aboutUs') ?>">ABOUT US
							<span class="sr-only">(current)</span>
						</a>
					</li>
					<li class="sep">|</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('user/howToOrder') ?>">HOW TO ORDER</a>
					</li>
					<li class="sep">|</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('user/gallery') ?>">GALLERY</a>
					</li>
					<li class="sep">|</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('user/contactUs') ?>">CONTACT US</a>
					</li>
				</ul>

				<button class="btn btn-light mr-1 nav-btn">
					<a href="<?= base_url('shopcart') ?>" class="shop-cart">
						<div>
							<img src="<?= base_url() ?>assets/image/asset/shopping-cart-151684_960_720.png" class="img-fluid" style="width: 25px; margin-top: -8px;">
						</div>
						<label class="account-init" style="margin-left: 5px; font-size: 14px;">10 Produk</label>
					</a>
				</button>

				<button class="btn btn-light mr-1 nav-btn">
					<a href="#">
						<div class="account-img rounded c-base">
							<img src="<?= base_url() ?>assets/image/asset/White Member.png">
						</div>
						<p style="color: var(--main-color); margin-left: 38px; margin-top: -30px;">Register</p>
					</a>
				</button>

				<button class="btn btn-light nav-btn" style="margin-right: 15px;">
					<a href="#">
						<div class="account-img rounded c-base">
							<img src="<?= base_url() ?>assets/image/asset/White Member.png" class="img-fluid">
						</div>
						<p class="account-name">Deddy</p>
						<p class="account-init">Logout</p>
					</a>
				</button>
			</div>
		</nav>

		<div class="arrow-back">
			<a href="<?= base_url('produk') ?>" class="left-arrow">
				<i class="fas fa-arrow-left"></i>
			</a>
		</div>
		<div class="notes"> DETAIL PRODUCT </div>
	</div>
	<br>

	<!-- content -->
	<div class="flex">

		<div id="sidebar" class="f-col-3 f-float-round mr-auto left-auto" style="background-color: white; height: auto; padding: 10px;">

			<div class="flex pad-sd mb-3">
				<div class="card">
					<div class="head-card">
						TEMA
					</div>
					<div class="content-card">
						<div class="label">
							<button class="btn btn-outline-danger w-100 active">ALL</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">GRADUATION</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">BIRTHDAY</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">ANYVERSARY</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">KID</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">EVENT</button>
						</div>
					</div>
				</div>
			</div>

			<div class="flex pad-sd mb-3">
				<div class="card">
					<div class="head-card">
						INSTAGRAM
					</div>
					<div class="content-card">

					</div>
				</div>
			</div>
		</div>

		<div class="f-col-9 content-product">
			<div class="flex-in mb-3" style="border-bottom: 1px solid #ddd">
				<div class="f-col">
					<div class="label">
						<h2 class="title-2">DETAIL PRODUCT</h2>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col-4 f-float-round pad-sm" style="margin-top: 5px;">
					<div>
						<img class="card-img-top" src="<?= base_url(). "assets/image/uploads_photo/produk/". $produk->gambar;?>" data-zoom-image="<?= base_url(). "assets/image/uploads_photo/produk/". $produk->gambar;?>" id="zoom">
					</div>
				</div>
				<div class="f-col-6 pad-sm">
					<div class="f-col f-float-round pad-sm">
						<div class="flex" style="padding-bottom: 0px !important; margin-bottom: 0px !important;">
							<div class="f-col">
								<div class="label">
									<h3 class="bold"><?= $produk->nama;?></h3>
									<div class="coral bold"><?= $produk->kode;?></div>
									<div class="label-4desc"><?= $produk->harga_jual ;?>-</div>
								</div>
							</div>
						</div>
						<div class="flex">
							<div class="f-col-9">
								<div class="label">
									<h5 class="label-4">Tema</h5>
									<div class="coral bold">Birthday</div>
								</div>
							</div>
							<div class="f-col-4">
								<div class="label">
									<h5 class="label-4">Status</h5>
									<div class="coral bold">Top Product</div>
								</div>
							</div>
						</div>
					</div>

					<div class="flex pad-sm" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;margin-bottom: 10px;">
						<div class="f-col">
							<div id="k1" class="bold">Keterangan</div>
						</div>
					</div>

					<div class="flex f-float-round pad-sm">
						<div class="f-col-9">
							<div class="label">
								<h5 class="bold">Keterangan </h5>
								<?= $produk->keterangan; 
								?>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="flex pad-sm f-float-round" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;">
				<div class="f-col">
					<div id="k1" class="bold">Additional Options</div>
				</div>
			</div>

			<div class="flex f-float-round pad-sm mb-3">
				<div class="f-col">
					<div class="flex">
						<table id='addtreload' class="table table-borderless" style="text-align: center;">
							<thead>
								<th>kode</th>
								<th>nama</th>
								<th>harga</th>
								<th>qty</th>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
					<form class="form-addt" data-uri='<?= site_url('User/Produk/addAddt');?>'>
					<div class="flex">
						<div class="f-col">
							Tambah Additional :
							<select class="form-control" name='additional'>
								<option></option>
								<?php foreach($addt as $row) : ?>
								<option value="<?= $row->tambahan_id ;?>-<?= $row->harga_jual;?>"><?= $row->name;?></option>
								<?php endforeach?>
							</select>
						</div>
						<div class="f-col-3">
							QTY : 
							<input type="qty" name="qty" class="form-control">
						</div>
						<div class="f-col-3">
							<button class="btn btn-primary" style="margin-top: 21px;">ADD</button>
						</div>
					</div>
					</form>
				</div>
			</div>

			<div class="flex pad-sm" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;margin-bottom: 10px;">
				<div class="f-col">
					<div class="bold">Komponen Produk</div>
				</div>
			</div>

			<div class="komponen">
				<?php foreach($komponen as $row) { ?>
				<div class="flex pad-sm f-float-round" style="background-color: #0482e3; color:white; border-radius: 5px; margin-top: 5px;">
					<div class="f-col">
						<div id="k1" class="bold"><?= $row->nama;?></div>
					</div>
				</div>

				<div class="flex f-float-round pad-sm mb-3">
					<div class="flex f-bordered">
						<div class="f-col" style="margin-top: 5px;">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" style="width: 150px; margin-top: 50px;">
							</div>
							<label class="label-product-desc" style="margin-left: 25%;font-size: 14px;"><?= $row->nama_komponen ;?></label>
						</div>
					</div>

					<div class="f-col-10" style="margin-left: 20px;">
						<div class="flex" style="padding-bottom: 0px !important; margin-bottom: 0px !important;">
							<div class="f-col">
								<div class="label">
									<h5 class="label">Pilihan Komponen</h5>
								</div>
								<textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="- Red"></textarea>
							</div>
						</div>
						<div class="f-col">
							<div class="label">
								<h5 class="label-4">Wording Big Balloon</h5>
							</div>
							<select name="" id="" class="form-control">
								<option value="">Pilih Font</option>
								<option value="">Arial</option>
								<option value="">Roboto</option>
							</select>
							<input type="text" class="form-control" placeholder="Tulis" style="margin-top: 15px">
						</div>
					</div>
				</div>
				<?php } ?>
			</div>

			<div class="flex">
				<div class="f-col">
					<div class="flex pad-sm f-float-round" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;">
						<div class="f-col">
							<div id="k1" class="bold">Special Note</div>
						</div>
					</div>
				</div>
				<div class="f-col">
					<div class="flex pad-sm f-float-round" style="background-color: #bd3232; color:white; border-radius: 5px; margin-top: 10px;">
						<div class="f-col">
							<div id="k1" class="bold">Important Notes</div>
						</div>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col">
					<textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="Tulis Catatan Khusus Untuk Pesanan Anda"></textarea>
				</div>
				<div class="f-col">
					<textarea name="" id="" cols="5" rows="5" class="form-control" placeholder="Tulis Catatan Khusus Untuk Pesanan Anda"></textarea>
				</div>
			</div>

			<div class="f-float-round padding-place" style="background-color: #0482e3;">
				<div class="flex place" style="margin-left: 60px;">
					<div class="f-col-4">
						<div class="label">
							<div style="font-size: 20px; color: white;" class="bold">JUMLAH PESANAN</div>
							<input style="padding: 20px;" type="text" name="jml_pesanan" id="jml_pesanan" placeholder="1" class="form-control">
						</div>
					</div>
					<div class="f-col-4">
						<div class="label">
							<div style="font-size: 20px; color: white;" class="bold">SUB TOTAL</div>
							<input style="padding: 20px;" type="text" name="qty" id="qty" placeholder="Rp. 250.000" class="form-control">
						</div>
					</div>
					<div class="f-col-4">
						<div class="label" style="padding-top: 20px;">
							<button id="submit" name="submit" class="btn btn-danger" style="color: white; padding: 15px;">ORDER NOW</button>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>

<?php
 
 	function set_curl($url)
 	{
	 	// persiapkan curl
	    $ch = curl_init(); 

	    // set url 
	    curl_setopt($ch, CURLOPT_URL, $url);

	    // return the transfer as a string 
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	    // $output contains the output string 
	    $output = curl_exec($ch); 

	    // tutup curl 
	    curl_close($ch);      

	    // menampilkan hasil curl
	    return json_decode($output, true);
 	}

 	$getimageUrl = set_curl('https://graph.instagram.com/me/media?fields=media_url,media_type&access_token=IGQVJXQzdNNnh2MS03ZAExkYS13Vi1zTWoxYk9sN0VXSWNVcHVwR2RZANy16OEIwVWRFWTZA3akY2RWtSZAUVhbENidDFvMVBGb282OHVlUFMwdzlSQXgwbGkzQ1g3Y3ZAKa1V1cHoxZAno2Q3haZAWFqMmlRMAZDZD');
 	$total = count($getimageUrl['data']);
?>
<style type="text/css">
	.image {
		width: 250px;
		height: 250px;
		float: left;
		margin-right: 20px;
		margin-bottom: 20px;
	}
	.image img {
		width: 100%;
	}
	.image video {
		width: 100%;
	}
</style>
<body id="page-top">
	<!-- Navigation -->
	<div class="header" style="background-image: url('<?= base_url() ?>assets/image/asset/bg1.jpg'); background-size: cover; background-repeat: no-repeat;">
		<nav class="navbar navbar-expand-lg navbar-dark navbar-ballooney static-top">

			<a class="navbar-brand" href="<?= base_url('user') ?>" style="margin-left: 20px;">
				<img src="<?= base_url() ?>assets/image/asset/logo.png">
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto my-auto">
					<li class="nav-item active">
						<a class="nav-link" href="<?= base_url('user/aboutUs') ?>">ABOUT US
							<span class="sr-only">(current)</span>
						</a>
					</li>
					<li class="sep">|</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('user/howToOrder') ?>">HOW TO ORDER</a>
					</li>
					<li class="sep">|</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('user/gallery') ?>">GALLERY</a>
					</li>
					<li class="sep">|</li>
					<li class="nav-item">
						<a class="nav-link" href="<?= base_url('user/contactUs') ?>">CONTACT US</a>
					</li>
				</ul>

				<?php if ($this->session->apps != 'balloney_user') : ?>
					<button onclick="showRegister()" class="btn btn-light mr-1 nav-btn">
						<div class="account-img rounded c-base register">
							<img src="<?= base_url() ?>assets/image/asset/White Member.png">
						</div>
						<p style="color: var(--main-color); margin-left: 38px; margin-top: -30px;">Register</p>
					</button>

					<button onclick="showLogin()" class="btn btn-light mr-1 nav-btn">
						<div class="account-img rounded c-base register">
							<img src="<?= base_url() ?>assets/image/asset/White Member.png">
						</div>
						<p style="color: var(--main-color); margin-left: 38px; margin-top: -30px;">Login</p>
					</button>
				<?php else : ?>
					<button class="btn btn-light mr-1 nav-btn">
						<a href="<?= base_url('shopcart') ?>" class="shop-cart">
							<div>
								<img src="<?= base_url() ?>assets/image/asset/shopping-cart-151684_960_720.png" class="img-fluid" style="width: 25px; margin-top: -8px;">
							</div>
							<label class="account-init" style="margin-left: 5px; font-size: 14px;">10
								Produk</label>
						</a>
					</button>

					<button class="btn btn-light mr-1 nav-btn">
						<a href="<?= base_url('myAccount/') . $this->session->pengguna_id; ?>">
							<div class="account-img rounded c-base register">
								<img src="<?= base_url() ?>assets/image/asset/White Member.png">
							</div>
							<p style="color: var(--main-color); margin-left: 38px; margin-top: -45px;">My Account</p>
						</a>
					</button>

					<button onclick="logout()" class="btn btn-light nav-btn" style="margin-right: 15px;">
						<div class="account-img rounded c-base">
							<img src="<?= base_url() ?>assets/image/asset/White Member.png" class="img-fluid">
						</div>
						<p class="account-name"><?= $this->session->username; ?></p>
						<p class="account-init">Logout</p>
					</button>
				<?php endif; ?>
			</div>
		</nav>

		<div class="header-title">
			<div class="note"> IT`S A SPECIAL DAYS FOR YOU </div>
			<div class="flex" style="margin-top: 10px;padding-top: 20px;padding-left: 10%;">
				<div class="f-col-5" id="col-select">
					<select class="search-home" name="" id="" style="font-weight: 400;">
						<option value="" selected disabled>Pilih Tema Produk Kesukaan Kamu</option>
					</select>
				</div>
				<div class="f-col-4">
					<button class="btn btn-primary btn-lg">CARI</button>
				</div>
			</div>
		</div>

	</div>
	<br>

	<!-- side bar -->
	<div class="flex">

		<div id="sidebar" class="f-col-3 f-float-round mr-auto left-auto" style="background-color: white; height: auto; padding: 10px;">

			<div class="flex pad-sd mb-3">
				<div class="card">
					<div class="head-card">
						TEMA
					</div>
					<div class="content-card">
						<div class="label">
							<button class="btn btn-outline-danger w-100 active">ALL</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">GRADUATION</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">BIRTHDAY</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">ANYVERSARY</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">KID</button>
						</div>
						<div class="label">
							<button class="btn btn-outline-danger w-100">EVENT</button>
						</div>
					</div>
				</div>
			</div>

			<div class="flex pad-sd mb-3">
				<div class="card">
					<div class="head-card">
						SOSIAL MEDIA
					</div>
					<div class="content-card">
						<div class="label">
							<button class="btn btn-outline-danger w-100 active">INSTAGRAM</button>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="f-col-9 content-product">
			<div class="flex">
				<div class="f-col">
			<?php
				for($no=0;$no < $total;$no++) {
					$data = $getimageUrl['data'][$no]['media_type'];
					$url = $getimageUrl['data'][$no]['media_url'];
					if($data == 'IMAGE')
					{
						echo "<div class='image'>
								<img src='". $url ."'>
							</div>";
					} else {
						echo "
							<div class='image'>
							<video width='320' height='240' controls>
						  		<source src='".$url."'>
							  </video>
							  </div>";
					}
				}
				;?>
				</div>
			</div>

		</div>
	</div>

	<!-- Modals -->
	<div id="register" class="modal-notif">
		<div class="modal-container" style="width: 450px;">
			<div class="modal-notif-header" style="background-color: red; padding-left: 30px;">
				<h4 style="color: #fff;">Register</h4>
				<p style="color: #fff;font-size: 16px;">Register your account in to the Ballooney Member Management System</p>
			</div>
			<div class="modal-notif-content pad-lg" style="margin-bottom: 0px !important">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your Name <span>*</span></label>
							<input type="text" name="name" id="name" class="form-control" placeholder="Input Name">
						</div>
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Phone Number <span>*</span></label>
							<input type="number" name="phone" id="phone" class="form-control" placeholder="Input Phone Number">
						</div>
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your Email <span>*</span></label>
							<input type="email" name="email" id="email" class="form-control" placeholder="Input Email">
						</div>
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your City <span>*</span></label>
							<input type="text" name="city" id="city" class="form-control" placeholder="Input City">
						</div>
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your Birthdate <span>*</span></label>
							<input type="date" name="tgl_lahir" id="tgl_lahir" class="form-control" placeholder="Input Birthdate">
						</div>
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your Username to login to the application <span>*</span></label>
							<input type="text" name="username" id="username" class="form-control" placeholder="Input Username">
						</div>
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your Password to login to the application <span>*</span></label>
							<input type="password" name="password" id="password" class="form-control" placeholder="Input Password">
						</div>

						<button onclick="register()" class="btn btn-primary pull-right mt-3" style="width: 100px;">REGISTER</button>
						<button onclick="hiddenRegister()" class="btn btn-danger pull-right mt-3" style="width: 100px;">BATAL</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="login" class="modal-notif">
		<div class="modal-container" style="width: 550px;">
			<div class="modal-notif-header" style="background-color: red; padding-left: 30px;">
				<h4 style="color: #fff;">Login</h4>
				<p style="color: #fff;font-size: 14px;">Enter your username and password to be able to log in to the Ballooney Member Management System</p>
			</div>
			<div class="modal-notif-content pad-lg" style="margin-bottom: 0px !important">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your Username to login to the application <span>*</span></label>
							<input type="text" name="username_login" id="username_login" class="form-control" placeholder="Input Username">
						</div>
						<div class="label">
							<label style="font-size: 14px; color:red;" class="head-form-control">Enter Your Password to login to the application <span>*</span></label>
							<input type="password" name="password_login" id="password_login" class="form-control" placeholder="Input Password">
						</div>

						<button onclick="login()" class="btn btn-primary pull-right mt-3" style="width: 100px;">LOGIN</button>
						<button onclick="hiddenLogin()" class="btn btn-danger pull-right mt-3" style="width: 100px;">BATAL</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		function register() {
			formdata = new FormData();
			formdata.append('name', $('#name').val());
			formdata.append('phone', $('#phone').val());
			formdata.append('email', $('#email').val());
			formdata.append('city', $('#city').val());
			formdata.append('tgl_lahir', $('#tgl_lahir').val());
			formdata.append('username', $('#username').val());
			formdata.append('password', $('#password').val());
			$('#loading').show();
			$.ajax({
				url: '<?php echo site_url('Auth/Authpengguna/simpan_pengguna'); ?>',
				data: formdata,
				type: "POST",
				dataType: 'JSON',
				contentType: false,
				cache: false,
				processData: false,
				success: function(response) {
					$('#loading').hide();
					if (response.status == 'ok') {
						Swal.fire({
							icon: 'success',
							title: 'Selamat !',
							text: 'Data berhasil ditambahkan!',
						});
						location.reload();
					} else if (response.status == 'nok') {
						Swal.fire({
							icon: 'error',
							title: 'Ups !',
							text: 'Data gagal ditambahkan!',
						});
						location.reload();
					}
				}
			})
		}

		function showRegister() {
			$('#register').show();
		}

		function hiddenRegister() {
			$('#register').hide();
		}

		function login() {
			var username = $('#username_login').val();
			var password = $('#password_login').val();
			$.ajax({
				url: '<?= site_url('Auth/Authpengguna/cek_validation'); ?>',
				data: {
					username: username,
					password: password
				},
				dataType: 'JSON',
				type: 'POST',
				success: function(response) {
					if (response.res == "success") {
						Swal.fire({
							icon: 'success',
							title: 'Selamat !',
							text: 'Anda berhasil Login!'
						});
						location.reload();
					} else {
						Swal.fire({
							icon: 'error',
							title: 'Ups !',
							text: 'Anda gagal Login!'
						});
						location.reload();
					}

				}
			})
		}

		function logout() {
			$.ajax({
				url: '<?= site_url('Auth/Authpengguna/do_logout'); ?>',
				dataType: 'JSON',
				success: function(response) {
					if (response.res == "success") {
						Swal.fire({
							icon: 'success',
							title: 'Selamat !',
							text: 'Anda berhasil Logout!'
						});
						location.reload();
					}
				}
			})
		}

		function showLogin() {
			$('#login').show();
		}

		function hiddenLogin() {
			$('#login').hide();
		}
	</script>

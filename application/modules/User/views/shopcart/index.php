<body class="dashboard-body">
	<div class="loading">
		<img src="<?= base_url() ?>assets/image/asset/loading.gif" alt="">
	</div>
	<div class="modal-item"></div>

	<div class="header-container">
		<div class="rounded c-trans">
			<div class="account-img c-base rounded">
				<label class="account-init"></label>
				<img src="<?= base_url() . "assets/image/profile_photo/" . $akun->photo; ?>" style="height: 100%;">
			</div>
			<label class="account-name"><?= $akun->nama; ?> / Member</label>
		</div>

		<a href="<?= base_url('user') ?>">
			<div class="rounded c-base pull-right store">
				<div class="account-img c-trans rounded">
					<img src="<?= base_url() ?>assets/image/asset/monitor red.png" style="background-size: cover; background-repeat: no-repeat; padding: 5px;">
				</div>
				<label class="account-name">STORE</label>
			</div>
		</a>

	</div>

	<div class="content-container">
		<div class="col col-content padding-content">
			<h4 class="bold">DAFTAR TRANSAKSI</h4>
			<div class="date-info f-red padding-tanggal running-date"></div><br />

			<div class="flex f-float-round pad-sm" style="margin-bottom: 13px;">
				<div class="f-col">
					<div class="flex">
						<div class="f-col-6">
							<div class="label">
								<h4 class="label-4">ID TRANSAKSI</h4>
								<button class="btn btn-jingga form-control" style="color: white;">TR_1234567889</button>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<h4 class="label-4">TRANSACTION DATE</h4>
								<button class="btn btn-jingga form-control" style="color: white;">Selasa, 24 Maret
									2020</button>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<h4 class="label-4">TRANSACTION TIME</h4>
								<button class="btn btn-jingga form-control" style="color: white;">09 : 00 : 15
									WIB</button>
							</div>
						</div>
					</div>
					<div class="flex">
						<div class="f-col-6">
							<div class="label">
								<h4 class="label-4">TOTAL TRANSAKSI</h4>
								<button class="btn btn-jingga form-control" style="color: white;">Rp. 350.000,-</button>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<h4 class="label-4">TOTAL PRODUK</h4>
								<button class="btn btn-jingga form-control" style="color: white;">02</button>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<h4 class="label-4">TRANSACTION STATUS</h4>
								<button class="btn btn-jingga form-control" style="color: white;">Order</button>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col-9 border-bottom">
					<div class="head-form-control darkred bold">PRODUK PESANAN</div>
				</div>
			</div>

			<div class="flex f-float-round pad-sm mb-3">
				<div class="f-col-3 f-float-round">
					<div class="image-round-sm">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-9">
					<div class="flex border-bottom">
						<div class="f-col-9">
							<div class="label">
								<h4 class="bold">PAKET BALON FOIL</h4>
								<div class="coral bold">BF 123165432</div>
							</div>
						</div>
						<div class="f-col-9 mr-2">
							<div class="label pull-right">
								<h5 class="label-4">Kode Transaksi Produk</h5>
								<div class="label-4desc">TRP_123165432</div>
							</div>
						</div>
					</div>
					<div class="flex mt-3">
						<div class="f-col-6">
							<div class="label">
								<div>Jumlah</div>
								<div class="darkred">1 Paket</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div>Harga</div>
								<div class="darkred">Rp. 250.000</div>
							</div>
						</div>
						<div class="f-col-3">
							<a href="<?= base_url('shopcart/detail') ?>">
								<button class="btn btn-primary btn-sm pull-right w70">DETAIL</button>
							</a>
						</div>
						<div class="f-col-3">
							<a href="<?= base_url('shopcart/edit') ?>">
								<button class="btn btn-warning btn-sm pull-right w70" style="color:white;">EDIT</button>
							</a>
						</div>
						<div class="f-col-3">
							<button class="btn btn-danger btn-sm pull-right w70">DELETE</button>
						</div>
					</div>
				</div>
			</div>

			<div class="flex f-float-round pad-sm mb-3">
				<div class="f-col-3 f-float-round">
					<div class="image-round-sm">
						<img src="<?= base_url() ?>assets/image/asset/Red Baloon.jpg" alt="">
					</div>
				</div>
				<div class="f-col-9">
					<div class="flex border-bottom">
						<div class="f-col-9">
							<div class="label">
								<h4 class="bold">PAKET BALON FOIL</h4>
								<div class="coral bold">BF 123165432</div>
							</div>
						</div>
						<div class="f-col-9 mr-2">
							<div class="label pull-right">
								<h5 class="label-4">Kode Transaksi Produk</h5>
								<div class="label-4desc">TRP_123165432</div>
							</div>
						</div>
					</div>
					<div class="flex mt-3">
						<div class="f-col-6">
							<div class="label">
								<div>Jumlah</div>
								<div class="darkred">1 Paket</div>
							</div>
						</div>
						<div class="f-col-6">
							<div class="label">
								<div>Harga</div>
								<div class="darkred">Rp. 250.000</div>
							</div>
						</div>
						<div class="f-col-3">
							<a href="<?= base_url('shopcart/detail') ?>">
								<button class="btn btn-primary btn-sm pull-right w70">DETAIL</button>
							</a>
						</div>
						<div class="f-col-3">
							<a href="<?= base_url('shopcart/edit') ?>">
								<button class="btn btn-warning btn-sm pull-right w70" style="color:white;">EDIT</button>
							</a>
						</div>
						<div class="f-col-3">
							<button class="btn btn-danger btn-sm pull-right w70">DELETE</button>
						</div>
					</div>
				</div>
			</div>

			<div class="flex">
				<div class="f-col-9 border-bottom">
					<div class="head-form-control darkred bold">PENGIRIMAN</div>
				</div>
			</div>

			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Metode Pengiriman</div>
							<select class="form-control" name="" id="">
								<option value="">Pilih Metode Pengiriman</option>
								<option selected value="">Diambil Sendiri</option>
								<option value="">Dikirim</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Waktu Rencana Pengiriman</div>
							<input type="text" class="form-control" placeholder="15 : 23 : 28 WIB">
						</div>
					</div>
				</div>
				<div class="flex">
					<div class="f-col-2">
						<a href="">
							<button class="btn btn-primary pull-right" style="width: 200px;">SUBMIT</button>
						</a>
					</div>
				</div>
			</div>

			<div class="flex" style="margin-top: 10px !important; padding-top: 0px !important;">
				<div class="f-col">
					<div class="darkred font-md border-bottom bold">PENGIRIMAN</div>
				</div>
			</div>
			<div class="f-float-round pad-sm">
				<div class="flex">
					<div class="f-col">
						<div class="label">
							<div class="head-form-control-2">Metode Pengiriman</div>
							<select class="form-control" name="" id="">
								<option selected value="">Dikirim</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Expedisi</div>
							<select class="form-control" name="" id="">
								<option value="">Pilih Expedisi</option>
								<option value="">Gojek</option>
								<option value="">Grab</option>
								<option value="">JNE</option>
								<option value="">JNT</option>
								<option value="">TIKI</option>
							</select>
						</div>
						<div class="label">
							<div class="head-form-control-2">Alamat Pengiriman</div>
							<input type="text" class="form-control" placeholder="Tulis Alamat">
						</div>
						<div class="label">
							<div class="head-form-control-2">Kontak Penerima</div>
							<input type="text" class="form-control" placeholder="Tulis Kontak Penerima">
						</div>
						<div class="label">
							<div class="head-form-control-2">Biaya</div>
							<input type="text" class="form-control" placeholder="Rp.">
						</div>
					</div>
				</div>
				<div class="flex">
					<div class="f-col-2">
						<a href="">
							<button class="btn btn-primary pull-right" style="width: 200px;">SUBMIT</button>
						</a>
					</div>
				</div>
			</div>

		</div>

		<div class="col col-menu pad-sm">
			<div class="menu-logo">
				<img src="<?= base_url() ?>assets/image/asset/logo.png" alt="">
			</div>
			<div class="menu-button">
				<a href="<?= base_url('shopcart') ?>" class="menu-item active">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Blue Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">SHOPPING CHART</div>
						<div class="menu-desc">Daftar Pembelian Produk Yang Anda Pilih</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('transaction') ?>" class="menu-item">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/Icon Transaksi.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">TRANSACTION</div>
						<div class="menu-desc">Melihat Seluruh Aktifitas Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<div class="menu-button">
				<a href="<?= base_url('history') ?>" class="menu-item">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/photo_default.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">HISTORY</div>
						<div class="menu-desc">Riwayat Transaksi Yang Telah Anda Lakukan</div>
					</div>
				</a>
			</div>
			<!-- <div class="menu-button">
				<a href="<?= base_url('myAccount') ?>" class="menu-item">
					<div class="menu-icon">
						<img src="<?= base_url() ?>assets/image/asset/document red.png" alt="">
					</div>
					<div class="menu-text">
						<div class="menu-title">MY ACCOUNT</div>
						<div class="menu-desc">Setting Akun Untuk Privasi Dan Kenyamanan Dalam Bertransaksi</div>
					</div>
				</a>
			</div> -->
		</div>
	</div>

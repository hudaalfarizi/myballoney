<?php defined('BASEPATH') or exit('No direct Access Allowed');

class Produk extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ProdukModel');
		$this->load->model('DashboardModel');
		$this->user_id = $this->session->id;
	}

	public function dashboard()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['token']        = $this->security->get_csrf_hash();
		$data['user'] = $this->DashboardModel->getData('public.pengguna', ['nama' => $this->session->userdata('nama')])->row_array();
		$data['main_content'] = 'User/dashboard/home';
		//get data produk + tema
		$data['produk']		  = $this->DashboardModel->get_data_produk()->result();
		$data['main_content'] = 'User/produk/index';
		$this->load->view('User/template', $data);
	}

	public function detailProduk($id)
	{
		$data['token']        = $this->security->get_csrf_hash();
		//tambah produk
		$data['produk']       = $this->ProdukModel->get_produk_detail($id);
		//tambah addt
		$data['addt']		  = $this->ProdukModel->get_data_addt($id);
		//komponen
		$data['komponen']     = $this->ProdukModel->get_data_komponen($id);
		$data['main_content'] = 'User/produk/detailProduk';
		$this->load->view('User/template',$data);
	}

	public function addAddt()
	{
		//nama produk
		$nama_pr     = htmlspecialchars($this->input->post('nama'));
		$id_pr       = htmlspecialchars($this->input->post('idproduk'));
		$explode	 = htmlspecialchars($this->input->post('additional'));
		$qty		 = htmlspecialchars($this->input->post('qty'));

		//explode
		$exp         = explode("-", $explode);
		$id_tambahan = $exp[0];
		$harga_addt  = $exp[1];

		$data = array(
			'produk_id'  => $id_pr,
			'nama_produk'=> $nama_pr,
			'user_ent'   => $this->user_id
		);
		$cek_kranjang = $this->db->get_where('transaksi.keranjang_produk',array('produk_id'=>$id_pr,'user_ent'=>$this->user_id));
		if($cek_kranjang->num_rows() == 0)
		{
			//simpan ke database produk id ini baru insert addt
			$this->db->insert('transaksi.keranjang_produk',$data);
			$idkode = $this->db->insert_id();
			$data2  = array(
				'keranjang_produk_kode' => $idkode,
				'produk_id'				=> $id_pr,
				'produk_tambahan_id'    => $id_tambahan,
				'harga'					=> $harga_addt,
				'qty'					=> $qty
			);
			$simpan = $this->db->insert('transaksi.keranjang_produk_tambahan',$data2);
			if($simpan)
			{
				$data['res'] = 'success';
			} else {
				$data['res'] = 'fail';
			}
		} else {
			//langsung simpan additional
			$keranjang_saya = $cek_kranjang->row();
			$kodeid			= $keranjang_saya->kode;
			$data3 = array(
				'keranjang_produk_kode' => $kodeid,
				'produk_id'				=> $id_pr,
				'produk_tambahan_id'    => $id_tambahan,
				'harga'					=> $harga_addt,
				'qty'					=> $qty
			);
			$simpan2 = $this->db->insert('transaksi.keranjang_produk_tambahan',$data3);
			if($simpan2)
			{
				$data['res'] = 'success';
			} else {
				$data['res'] = 'fail';
			}
		}

		echo json_encode($data);
	}
}

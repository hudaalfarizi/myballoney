<?php defined('BASEPATH') or exit('No direct Access Allowed');

class Myaccount extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('MyaccountModel');
		$data['msg'] = '';
		$data['url'] = '';
		$data['res'] = '';
	}

	public function dashboard()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->MyaccountModel->get_data();
		$data['akun_2'] = $this->MyaccountModel->get_data_2();
		$data['main_content'] = 'User/myaccount/index';
		$this->load->view('Shopcart/template', $data);
	}

	function simpan_edit_member()
	{
		$feedback['status'] = '';
		// $options = [
		// 	'cost' => 12,
		// ];
		$id = $this->input->post('id');
		$name = htmlspecialchars($this->input->post('nama'));
		$email = htmlspecialchars($this->input->post('email'));
		$tlp = htmlspecialchars($this->input->post('phone'));
		$alamat = htmlspecialchars($this->input->post('alamat'));
		$tgl_lahir = htmlspecialchars($this->input->post('tgl_lahir'));
		$tgl_bergabung = htmlspecialchars($this->input->post('tgl_bergabung'));
		$gender = htmlspecialchars($this->input->post('gender'));
		$username = htmlspecialchars($this->input->post('username'));
		$password = htmlspecialchars($this->input->post('password'));
		// $password = password_hash($this->input->post('password'), PASSWORD_BCRYPT, $options);

		$data = array(
			'nama' => $name,
			'email' => $email,
			'no_telp' => $tlp,
			'tanggal_lahir' => $tgl_lahir,
			'alamat' => $alamat,
			'tanggal_pendaftaran' => $tgl_bergabung,
			'jenis_kelamin' => $gender
		);
		$this->db->where('id', $id);
		$update = $this->db->update('pengguna', $data);

		$data_2 = [
			'username' => $username,
			'password' => $password
		];
		$this->db->where('pengguna_id', $id);
		$update = $this->db->update('pengguna_akun', $data_2);
		if ($update) {
			$feedback['status'] = 'success';
		} else {
			$feedback['status'] = 'nok';
		}

		echo json_encode($feedback);
	}

	public function updatephoto()
	{
		$data['token'] = $this->security->get_csrf_hash();
		$upload_foto = upload_photo_profil($_FILES);
		$data = $upload_foto['file_name'];
		if (!$data) {
			$response = [
				'status' => 'nok',
				'error' => $this->upload->display_errors()
			];
			echo json_encode($response);
		} else {
			$this->MyaccountModel->save_foto($data);
			$response = [
				'status' => 'ok'
			];
			echo json_encode($response);
		}
	}

	public function updatePengguna()
	{
		$data['akun'] = $this->MyaccountModel->get_data();
		$post = $this->input->post(null, true);
		$data = [
			'id' => $post['id'],
			'nama'      => $post['nama'],
			'email'     => $post['email'],
			'no_telp'   => $post['phone'],
			'jenis_kelamin' 	=> $post['gender'],
			'tanggal_lahir' 	=> $post['tgl_lahir'],
			'tanggal_pendaftaran' 	=> $post['tgl_bergabung'],
			'alamat' 	=> $post['alamat']
		];
		$simpan = $this->MyaccountModel->update_member('public.pengguna', $data);
		$data_2 = [
			'pengguna_id' => $post['id'],
			'username'  => $post['username'],
			'password' 	=> $post['password']
		];
		$simpan = $this->MyaccountModel->update_member_2('public.pengguna_akun', $data_2);
		if ($simpan == TRUE) {
			$data['msg'] = 'Data berhasil di update';
			$data['url'] = site_url('user');
			$data['res'] = 'success';
			$data['status'] = 'success';
		} else {
			$data['msg'] = 'Data gagal di update';
			$data['url'] = site_url('user');
			$data['res'] = 'gagal';
			$data['status'] = 'fail';
		}
		echo json_encode($data);
	}

	public function update_photo()
	{
		$data['token'] = $this->security->get_csrf_hash();
		$upload_foto = upload_photo_profil($_FILES);
		$data = $upload_foto['file_name'];
		if (!$data) {
			$response = [
				'status' => 'nok',
				'error' => $this->upload->display_errors()
			];
			echo json_encode($response);
		} else {
			$this->MyaccountModel->save_foto($data);
			$response = [
				'status' => 'ok'
			];
			echo json_encode($response);
		}
	}
}

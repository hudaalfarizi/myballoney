<?php defined('BASEPATH') or exit('No direct Access Allowed');

class Transaction extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('TransactionModel');
	}

	public function dashboard()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->TransactionModel->get_data();
		$data['akun_2'] = $this->TransactionModel->get_data_2();
		$data['main_content'] = 'User/transaction/index';
		$this->load->view('Shopcart/template', $data);
	}

	public function transactionMenunggu()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->TransactionModel->get_data();
		$data['akun_2'] = $this->TransactionModel->get_data_2();
		$data['main_content'] = 'User/transaction/transactionMenunggu';
		$this->load->view('Shopcart/template', $data);
	}

	public function transactionPembayaran()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->TransactionModel->get_data();
		$data['akun_2'] = $this->TransactionModel->get_data_2();
		$data['main_content'] = 'User/transaction/transactionPembayaran';
		$this->load->view('Shopcart/template', $data);
	}

	public function transactionProses()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->TransactionModel->get_data();
		$data['akun_2'] = $this->TransactionModel->get_data_2();
		$data['main_content'] = 'User/transaction/transactionProses';
		$this->load->view('Shopcart/template', $data);
	}

	public function transactionDetail()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->TransactionModel->get_data();
		$data['akun_2'] = $this->TransactionModel->get_data_2();
		$data['main_content'] = 'User/transaction/transactionDetail';
		$this->load->view('Shopcart/template', $data);
	}

	public function transactionEdit()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->TransactionModel->get_data();
		$data['akun_2'] = $this->TransactionModel->get_data_2();
		$data['main_content'] = 'User/transaction/transactionEdit';
		$this->load->view('Shopcart/template', $data);
	}
}

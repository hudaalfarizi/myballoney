<?php defined('BASEPATH') or exit('No direct Access Allowed');

class History extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('HistoryModel');
	}

	public function dashboard()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->HistoryModel->get_data();
		$data['akun_2'] = $this->HistoryModel->get_data_2();
		$data['main_content'] = 'User/history/index';
		$this->load->view('Shopcart/template', $data);
	}

	public function historySelesai()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->HistoryModel->get_data();
		$data['akun_2'] = $this->HistoryModel->get_data_2();
		$data['main_content'] = 'User/history/historySelesai';
		$this->load->view('Shopcart/template', $data);
	}

	public function historySelesaiDetail()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->HistoryModel->get_data();
		$data['akun_2'] = $this->HistoryModel->get_data_2();
		$data['main_content'] = 'User/history/historySelesaiDetail';
		$this->load->view('Shopcart/template', $data);
	}
}

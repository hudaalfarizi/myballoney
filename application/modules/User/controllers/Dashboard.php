<?php defined('BASEPATH') or exit('No direct Access Allowed');

class Dashboard extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('DashboardModel');
	}

	public function dashboard()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['user'] = $this->DashboardModel->getData('public.pengguna', ['nama' => $this->session->userdata('nama')])->row_array();
		$data['main_content'] = 'User/dashboard/home';
		//get data produk + tema
		$data['produk']		  = $this->DashboardModel->get_data_produk()->result();
		$this->load->view('User/template', $data);
	}

	public function aboutUs()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['result'] = $this->DashboardModel->get_data_About('public.halaman_website');
		$data['main_content'] = 'User/dashboard/aboutUs';
		$this->load->view('User/template', $data);
	}

	public function howToOrder()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['result'] = $this->DashboardModel->get_data_Howtoorder('public.halaman_website');
		$data['main_content'] = 'User/dashboard/howToOrder';
		$this->load->view('User/template', $data);
	}

	public function gallery()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['result'] = $this->DashboardModel->get_data_Gallery('public.halaman_website');
		$data['main_content'] = 'User/dashboard/gallery';
		$this->load->view('User/template', $data);
	}

	public function contactUs()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['halaman'] = $this->DashboardModel->get_data_Halamancontact('public.halaman_website');
		$data['result'] = $this->DashboardModel->get_data_Contactus('public.profil_website');
		$data['main_content'] = 'User/dashboard/contactUs';
		$this->load->view('User/template', $data);
	}

	public function instagram()
	{
		$data['main_content'] = 'User/dashboard/instagram';
		$this->load->view('User/template',$data);
	}
}

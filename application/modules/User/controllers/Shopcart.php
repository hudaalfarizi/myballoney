<?php defined('BASEPATH') or exit('No direct Access Allowed');

class Shopcart extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('ShopcartModel');
	}

	public function dashboard()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->ShopcartModel->get_data();
		$data['akun_2'] = $this->ShopcartModel->get_data_2();
		$data['main_content'] = 'User/shopcart/index';
		$this->load->view('Shopcart/template', $data);
	}

	public function detailShopCart()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->ShopcartModel->get_data();
		$data['akun_2'] = $this->ShopcartModel->get_data_2();
		$data['main_content'] = 'User/shopcart/detailShopCart';
		$this->load->view('Shopcart/template', $data);
	}

	public function editShopCart()
	{
		$data['token']        = $this->security->get_csrf_hash();
		$data['akun'] = $this->ShopcartModel->get_data();
		$data['akun_2'] = $this->ShopcartModel->get_data_2();
		$data['main_content'] = 'User/shopcart/editShopCart';
		$this->load->view('Shopcart/template', $data);
	}
}

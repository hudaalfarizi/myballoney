get_addt();
$('.form-balloney').submit(function () {
	$.ajax({
		type: 'post',
		url: $(this).data('uri'),
		data: $(this).serialize(),
		dataType: 'json',
		beforeSend: function () {
			loading('show');
		},
		success: function (data) {
			if (data.res == 'success') {
				Swal.fire({
					icon: 'success',
					title: 'Selamat !',
					text: data.msg
				});
				window.location = data.url;
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Ups !',
					text: data.msg
				});
				window.location = data.url;
			}
		},
		error: function () {
			Swal.fire({
				icon: 'error',
				title: 'Ups !',
				text: 'Network error!'
			});
			window.location = data.url;
		}
	});
	return false;
});

$('.form-addt').submit(function () {
	$.ajax({
		type: 'post',
		url: $(this).data('uri'),
		data: $('.form-addt, .form-construct').serialize(),
		dataType: 'json',
		beforeSend: function () {
			loading('show');
		},
		success: function (data) {
			if (data.res == 'success') {
				$("#transaksi").load(" #transaksi");
				get_addt();
			}
		},
		error: function () {
			Swal.fire({
				icon: 'error',
				title: 'Ups !',
				text: 'Network error!'
			});
			window.location = data.url;
		}
	});
	return false;
});

function get_addt()
{
	$.ajax({
		url  	 : '<?= site_url("User/Produk/get_addt");?>',
		dataType : 'JSON',
		success  : function(response)
		{
			$.each(response, function(i,val){
				$('#addtreload').append("<tr><td>"+ val.kode +"</td><td>"+ val.name +"</td><td>"+ val.harga_jual +"</td><td>"+ val.qty +"</td></tr>")
			});
		}
	})
}

$('.form-balloney-plus').submit(function () {
	var id = $('#idp').val();
	var form = $(this).serialize();
	$.ajax({
		type: 'post',
		url: $(this).data('uri'),
		data: form + "&id=" + id,
		dataType: 'json',
		beforeSend: function () {
			loading('show');
		},
		success: function (data) {
			if (data.res == 'success') {
				Swal.fire({
					icon: 'success',
					title: 'Selamat !',
					text: data.msg
				});
				window.location = data.url;
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Ups !',
					text: data.msg
				});
				window.location = data.url;
			}
		},
		error: function () {
			Swal.fire({
				icon: 'error',
				title: 'Ups !',
				text: 'Network error!'
			});
		}
	});
	return false;
});


$('.delete').on("click", function () {
	idkode = $(this).attr('idkode');
	csrf_apd = $(this).data('token');
	var confirmdelete = confirm("Apakah Anda Yakin ingin menghapus data ?");
	if (confirmdelete) {
		$.ajax({
			type: 'post',
			url: $('.delete').data('uri'),
			data: {
				idkode: idkode,
				csrf_apd: csrf_apd
			},
			dataType: 'json',
			beforeSend: function () {
				loading('show');
			},
			success: function (data) {
				if (data.res == 'success') {
					Swal.fire({
						icon: 'success',
						title: 'Selamat !',
						text: data.msg
					});
					window.location = data.url;
				} else {
					Swal.fire({
						icon: 'error',
						title: 'Ups !',
						text: data.msg
					});
					window.location = data.url;
				}
			},
			error: function () {
				Swal.fire({
					icon: 'error',
					title: 'Ups !',
					text: 'Network error!'
				});
				window.location = data.url;
			}
		});
		return false;
	}
});


$('#iyaapus').on("click", function () {
	$("#del-act").hide()
	kode_gudang = $('.delete').attr('idkode');
	csrf_apd = $('.delete').data('token');
	$.ajax({
		type: 'post',
		url: $('.delete').data('uri'),
		data: {
			kode_gudang: kode_gudang,
			csrf_apd: csrf_apd
		},
		dataType: 'json',
		beforeSend: function () {
			loading('show');
		},
		success: function (data) {
			if (data.res == 'success') {
				Swal.fire({
					icon: 'success',
					title: 'Selamat !',
					text: data.msg
				});
				//window.location = data.url;
			} else {
				Swal.fire({
					icon: 'error',
					title: 'Ups !',
					text: data.msg
				});
				//window.location = data.url;
			}
		},
		error: function () {
			Swal.fire({
				icon: 'error',
				title: 'Ups !',
				text: 'Network error!'
			});
			window.location = data.url;
		}
	});
	return false;
});
$("#gajadi").on("click", function () {
	$("#del-act").hide();
});
